--   DROP DATABASE `thisbank`;
	-- create database
	 CREATE DATABASE `thisbank` /*!40100 DEFAULT CHARACTER SET latin1 */;
	 
	 

	
		-- create people table
	CREATE TABLE `thisbank`.`customers` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(150) NOT NULL,
    `last_name` VARCHAR(150) NOT NULL,
    `dob` DATE NOT NULL,
    `email` VARCHAR(150) NOT NULL,
    `phone` VARCHAR(13) NULL,
    `mobile` VARCHAR(12) NULL,
    `active` BOOLEAN NOT NULL,
    PRIMARY KEY (`id`)
);

	-- Create Users table
	CREATE TABLE `thisbank`.`users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `role` VARCHAR(20) NOT NULL,
    `username` VARCHAR(50) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `created` DATETIME,
    `modified` DATETIME,
    `customer_id` INT NULL,
    PRIMARY KEY (`id`),
    INDEX `CustomerID_idx` (`customer_id` ASC),
    CONSTRAINT `UserCustomerID` FOREIGN KEY (`customer_id`)
        REFERENCES `thisbank`.`customers` (`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT
);	


	-- Create Addresses table
	CREATE TABLE `thisbank`.`customer_addresses` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `customer_id` INT NOT NULL,
    `address_line_1` VARCHAR(200) NOT NULL,
    `suburb` VARCHAR(200) NULL,
    `post_code` VARCHAR(4) NULL,
    `state` VARCHAR(3),
    PRIMARY KEY (`id`),
    INDEX `CustomerID_idx` (`customer_id` ASC),
    CONSTRAINT `AddressCustomerID` FOREIGN KEY (`customer_id`)
        REFERENCES `thisbank`.`customers` (`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT
);
	  
	 -- Create Transaction Types table
	CREATE TABLE `thisbank`.`transaction_types` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(200),
    PRIMARY KEY (`id`)
);
	  
	 -- Create Branches table
	CREATE TABLE `thisbank`.`branches` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200),
    `phone` VARCHAR(13),
    PRIMARY KEY (`id`)
);
	  
	 -- Create Account Types
	CREATE TABLE `thisbank`.`account_types` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(200),
    PRIMARY KEY (`id`)
);

	   -- Create Account Types
	CREATE TABLE `thisbank`.`interests` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `account_type_id` INT NOT NULL,
    `start_date` DATETIME,
    `end_date` DATETIME,
    `interest` DECIMAL(5 , 4 ),
    `description` VARCHAR(200),
    PRIMARY KEY (`id`),
    INDEX `AccountType_idx` (`account_type_id` ASC),
    CONSTRAINT `AccountTypeInterest` FOREIGN KEY (`account_type_id`)
        REFERENCES `thisbank`.`account_types` (`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT
);
	  
	-- Create Accounts
	
	CREATE TABLE `thisbank`.`accounts` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `branch_id` INT(11) NOT NULL,
    `account_description` VARCHAR(150) DEFAULT NULL,
    `active` TINYINT(1) NOT NULL,
    `open_date` DATETIME NOT NULL,
    `account_type_id` INT(11) NOT NULL,
    `customer_id` INT(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `BranchAccount_idx` (`branch_id`),
    KEY `CustomerAccount_idx` (`customer_id`),
    KEY `AccountTypeAccount_idx` (`account_type_id`),
    CONSTRAINT `AccountTypeAccount` FOREIGN KEY (`account_type_id`)
        REFERENCES `thisbank`.`account_types` (`id`),
    CONSTRAINT `BranchAccount` FOREIGN KEY (`branch_id`)
        REFERENCES `thisbank`.`branches` (`id`),
    CONSTRAINT `CustomerAccount` FOREIGN KEY (`customer_id`)
        REFERENCES `thisbank`.`customers` (`id`)
)  ENGINE=INNODB AUTO_INCREMENT=17 DEFAULT CHARSET=LATIN1;


	-- Create Transactions

	CREATE TABLE `thisbank`.`transactions` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `transaction_type_id` INT,
    `account_id` INT,
    `transaction_date` DATETIME,
    `amount` DECIMAL(13 , 4 ),
    description VARCHAR(100),
    destination_account INT NULL,
    PRIMARY KEY (`id`),
    INDEX `TransactionType_idx` (`transaction_type_id` ASC),
    INDEX `TransactionAccount_idx` (`account_id` ASC),
    CONSTRAINT `TransactionType` FOREIGN KEY (`transaction_type_id`)
        REFERENCES `thisbank`.`transaction_types` (`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `DestinationAccount` FOREIGN KEY (`destination_account`)
        REFERENCES `thisbank`.`accounts` (`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `TransactionAccount` FOREIGN KEY (`account_id`)
        REFERENCES `thisbank`.`accounts` (`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT
);
	  
	
			


use `thisbank`;

-- INSERT SOME VALUES INTO THE DATABASE
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Rose', 'Cook', '1945-11-07', 'rcook0@google.com.br', '0291313954', '0404167868', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Mary', 'Robertson', '1926-01-17', 'mrobertson1@liveinternet.ru', '0233649998', '0415938999', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Johnny', 'Hanson', '1933-10-24', 'jhanson2@unicef.org', '0744443333', '0409330000', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Paula', 'George', '1959-01-17', 'pgeorge3@usa.gov', '0741524725', '0409303000', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Craig', 'Lynch', '1975-05-02', 'clynch4@mozilla.com', '0741524309', '0403099888', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Phillip', 'Gilbert', '1977-08-10', 'pgilbert5@artisteer.com', '0740310339', '0490000999', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Earl', 'Mccoy', '1950-02-27', 'emccoy6@zdnet.com', '0740310339', '0420030999', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Ronald', 'Perkins', '1966-08-28', 'rperkins7@etsy.com', '0385593993', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Teresa', 'Bailey', '1978-02-23', 'tbailey8@qq.com', '0743905555', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Robert', 'Bryant', '1921-05-07', 'rbryant9@tmall.com', '0294339689', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Michael', 'Graham', '1929-12-17', 'mgrahama@weather.com', '0733320049', '0403000211', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Eugene', 'Dunn', '1971-09-23', 'edunnb@is.gd', '0383495550', '0415153095', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Ryan', 'Greene', '1991-02-12', 'rgreenec@omniture.com', '0747995233', '0419930952', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Wanda', 'Sullivan', '1928-11-11', 'wsullivand@nature.com', '0885903390', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Julie', 'Robinson', '1974-07-07', 'jrobinsone@bing.com', '0731529932', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Howard', 'Fox', '1924-07-18', 'hfoxf@wikispaces.com', '', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Paul', 'Wilson', '1994-08-07', 'pwilsong@nifty.com', '', '0427950499', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Denise', 'Hart', '1922-01-01', 'dharth@vkontakte.ru', '', '0423490903', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Robert', 'Clark', '1921-10-12', 'rclarki@studiopress.com', '0294220099', '0402003095', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Louise', 'Woods', '1928-08-23', 'lwoodsj@ycombinator.com', '', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Mildred', 'Montgomery', '1981-04-06', 'mmontgomeryk@examiner.com', '', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Shirley', 'Watson', '1990-12-19', 'swatsonl@cyberchimps.com', '', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Catherine', 'Olson', '1972-01-29', 'colsonm@meetup.com', '', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Margaret', 'Griffin', '1939-11-14', 'mgriffinn@economist.com', '0743090958', '0490023102', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Lillian', 'Jacobs', '1961-07-14', 'ljacobso@myspace.com', '029490-0876', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Jacqueline', 'Allen', '1945-03-15', 'jallenp@purevolume.com', '', '0430493-085', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Shirley', 'Evans', '1934-09-21', 'sevansq@xinhuanet.com', '', '0428088983', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Todd', 'Mitchell', '1995-03-27', 'tmitchellr@odnoklassniki.ru', '0733280932', '0402999182', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Linda', 'Rogers', '1922-11-12', 'lrogerss@harvard.edu', '0743930455', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Kenneth', 'Long', '1924-02-22', 'klongt@huffingtonpost.com', '0731523332', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Julie', 'Cooper', '1991-06-20', 'jcooperu@businesswire.com', '0731569998', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Todd', 'Garcia', '1943-02-01', 'tgarciav@nasa.gov', '', '0413444383', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Gloria', 'Williams', '1940-03-19', 'gwilliamsw@hhs.gov', '0733380844', '0415839493', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Mary', 'Lawrence', '1934-05-23', 'mlawrencex@nymag.com', '0733210983', '0409031819', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Laura', 'Rivera', '1982-07-14', 'lriveray@ucoz.ru', '0743392987', '0435220784', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Wayne', 'Rogers', '1933-07-02', 'wrogersz@whitehouse.gov', '0733211229', '0403004358', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Maria', 'Gilbert', '1960-02-23', 'mgilbert10@huffingtonpost.com', '', '0443416611', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Maria', 'Cole', '1933-03-25', 'mcole11@sphinn.com', '0743392928', '04329040625', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Teresa', 'Pierce', '1981-10-23', 'tpierce12@ask.com', '073321-0887', '0477193994', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Fred', 'Rice', '1929-11-17', 'frice13@shutterfly.com', '0743392983', '0433137789', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Dennis', 'Hayes', '1973-05-01', 'dhayes14@dot.gov', '0745124389', '0470692127', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Douglas', 'Mendoza', '1962-02-13', 'dmendoza15@bloomberg.com', '0743392939', '0406706284', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Raymond', 'Ross', '1956-09-22', 'rross16@sciencedirect.com', '0733210988', '04285993718', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Ruth', 'Black', '1935-02-26', 'rblack17@amazon.com', '0743392982', '0428989361', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Kathleen', 'Stewart', '1957-08-14', 'kstewart18@privacy.gov.au', '0733210283', '0403229183', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Amy', 'Watkins', '1985-07-27', 'awatkins19@deliciousdays.com', '', '04807964120', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Jeffrey', 'Jones', '1968-03-06', 'jjones1a@cornell.edu', '0733210938', '0448583438', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Arthur', 'Jordan', '1975-07-02', 'ajordan1b@washingtonpost.com', '0733211111', '0479856151', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Carlos', 'Austin', '1943-04-29', 'caustin1c@over-blog.com', '0743392982', '0454343269', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Scott', 'Walker', '1928-10-08', 'swalker1d@baidu.com', '0733210987', '0403229119', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Antonio', 'Lee', '1994-05-07', 'alee1e@aol.com', '0743392982', '0440277583', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Janice', 'Nelson', '1967-02-14', 'jnelson1f@simplemachines.org', '0733210938', '0403229283', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Jimmy', 'Duncan', '1964-07-09', 'jduncan1g@sohu.com', '0655040048', '0495141141', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Steve', 'Richardson', '1995-10-07', 'srichardson1h@hhs.gov', '0733210123', '0416850437', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Earl', 'Greene', '1986-02-21', 'egreene1i@state.tx.us', '0290929387', '0411242654', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Anna', 'Rice', '1970-10-29', 'arice1j@yellowpages.com', '0745994983', '0403229193', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Kathleen', 'Oliver', '1930-07-24', 'koliver1k@shutterfly.com', '0733210112', '0446897836', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Wayne', 'Hawkins', '1974-07-07', 'whawkins1l@time.com', '0290929298', '0403229181', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Paul', 'Robinson', '1964-12-18', 'probinson1m@usgs.gov', '0290921928', '04553933826', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Laura', 'Barnes', '1954-02-24', 'lbarnes1n@jimdo.com', '0745994929', '0403229183', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Joe', 'Cole', '1989-07-12', 'jcole1o@pen.io', '0733210388', '04402655379', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Larry', 'Frazier', '1941-09-22', 'lfrazier1p@yahoo.com', '0290929328', '04710460286', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Carol', 'Anderson', '1935-10-21', 'canderson1q@yandex.ru', '0745991938', '0403292428', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Rachel', 'Wilson', '1952-06-09', 'rwilson1r@ucoz.ru', '0733210229', '0472589688', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Bobby', 'Chavez', '1928-06-09', 'bchavez1s@istockphoto.com', '0290929192', '0403292418', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Steven', 'Morgan', '1978-05-02', 'smorgan1t@amazon.com', '0745994288', '0450297825', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Brian', 'Cruz', '1967-11-04', 'bcruz1u@tumblr.com', '0733212938', '0406809818', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Margaret', 'Ortiz', '1933-06-27', 'mortiz1v@google.de', '0290929298', '0403292410', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Nicholas', 'Miller', '1945-06-20', 'nmiller1w@bravesites.com', '0745994287', '0433662299', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Bonnie', 'Garcia', '1979-07-22', 'bgarcia1x@engadget.com', '0733210182', '0471014740', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Andrea', 'Morgan', '1958-12-31', 'amorgan1y@imageshack.us', '0745994388', '0403292411', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Brenda', 'Howell', '1978-01-23', 'bhowell1z@bloglines.com', '0290921938', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Helen', 'Wright', '1932-07-16', 'hwright20@msn.com', '0733210921', '0403292491', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Eugene', 'Lewis', '1962-01-19', 'elewis21@miibeian.gov.cn', '0290929329', '0403293292', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Clarence', 'Allen', '1955-02-21', 'callen22@studiopress.com', '0745994187', '0403293291', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Amy', 'Ellis', '1957-01-21', 'aellis23@ihg.com', '', '0244433987', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Christopher', 'Fisher', '1941-09-23', 'cfisher24@google.ru', '0733210192', '0403293382', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Brenda', 'Alexander', '1939-04-26', 'balexander25@apple.com', '0290929210', '0405037282', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Christine', 'Morris', '1922-06-02', 'cmorris26@msu.edu', '0733210192', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Jesse', 'Perez', '1994-09-12', 'jperez27@woothemes.com', '0745994287', '0405038382', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Christina', 'Owens', '1946-03-19', 'cowens28@phpbb.com', '0733210210', '0405004387', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Jason', 'Edwards', '1983-03-06', 'jedwards29@ft.com', '0290929389', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Stephanie', 'Alexander', '1971-05-31', 'salexander2a@nba.com', '0290922275', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Stephen', 'Graham', '1967-03-22', 'sgraham2b@msn.com', '0298718207', '0430099123', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Paula', 'Rodriguez', '1944-03-20', 'prodriguez2c@skype.com', '0743212944', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Craig', 'Wagner', '1938-09-08', 'cwagner2d@ucoz.ru', '0745994928', '0435393382', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Louise', 'Evans', '1987-01-03', 'levans2e@list-manage.com', '', '0439156069', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Catherine', 'Phillips', '1992-07-19', 'cphillips2f@china.com.cn', '', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Scott', 'Washington', '1967-06-18', 'swashington2g@booking.com', '0291523331', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Sean', 'Welch', '1933-11-03', 'swelch2h@pcworld.com', '', '0403004358', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Brenda', 'Reynolds', '1988-12-19', 'breynolds2i@bandcamp.com', '', '0427636548', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Donna', 'Larson', '1932-04-01', 'dlarson2j@home.pl', '0294031929', '0415157708', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Clarence', 'Murray', '1960-10-16', 'cmurray2k@mayoclinic.com', '', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Joe', 'Flores', '1923-08-06', 'jflores2l@blogger.com', '', '0405004555', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Henry', 'Wells', '1959-12-18', 'hwells2m@google.pl', '', '0411112301', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Eric', 'Brown', '1935-11-29', 'ebrown2n@intel.com', '0293330444', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Bobby', 'Mitchell', '1927-02-13', 'bmitchell2o@clickbank.net', '0741524000', '0403005111', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Paula', 'Lewis', '1971-08-17', 'plewis2p@newyorker.com', '0291443339', '', 0);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Dorothy', 'Harris', '1920-02-15', 'dharris2q@go.com', '0734430000', '', 1);
insert into customers (first_name, last_name, dob, email, phone, mobile, active) values ('Kevin', 'Stewart', '1976-03-06', 'kstewart2r@narod.ru', '0291103993', '', 1);


-- ADDRESSES

insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (1,'55 Meadow Ridge Point', 3451, 'VIC', 'Bendigo');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (2,'6031 Clemons Road', 4205, 'QLD', 'Bethania');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (3,'6553 Oneill Pass', 2487, 'NSW', 'Chinderah');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (4,'27 Spenser Parkway', 4155, 'QLD', 'Chandler');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (5,'4229 Sauthoff Parkway', 2203, 'NSW', 'Dulwich Hill');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (6,'2551 Vernon Drive', 2485, 'NSW', 'Tweed Heads');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (7,'799 Emmet Trail', 2029, 'NSW', 'Beverley Hills');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (8,'12 Esker Alley', 4301, 'QLD', 'Goodna');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (9,'72 Northwestern Avenue', 3442, 'VIC', 'Sunbury');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (10,'997 Lotheville Lane', 2482, 'NSW', 'Mullumbimby');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (11,'90237 Myrtle Road', 4285, 'QLD', 'Beaudesert');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (12,'618 Green Pass', 2122, 'NSW', 'Eastwood');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (13,'950 Debra Drive', 4579, 'QLD', 'Rockhampton');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (14,'25620 Fuller Hill', 4621, 'QLD', 'Gayndah');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (15,'08383 Spenser Point', 2472, 'NSW', 'Lismore');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (15,'3 Pine View Drive', 4127, 'QLD', 'Beenleigh');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (16,'31 Melvin Parkway', 4055, 'QLD', 'Ferny Hills');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (16,'2360 Fairview Pass', 4305, 'QLD', 'Bundambah');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (17,'41 Kinsman Park', 4763, 'QLD', 'Townsville');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (18,'6 Carey Trail', 2299, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (19,'620 Algoma Place', 4760, 'QLD', 'Bundaberg');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (20,'2 Forest Avenue', 4592, 'QLD', 'Rockhampton');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (22,'49381 Commercial Park', 4079, 'QLD', 'Nundah');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (22,'499 Carberry Street', 3030, 'VIC', 'Beenleigh');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (24,'65080 Lien Center', 4762, 'QLD', 'Kerwin');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (25,'8423 Susan Trail', 3687, 'VIC', 'Melbourne');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (26,'54781 Declaration Place', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (27,'6 Boyd Street', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (28,'508 Marcy Hill', 2121, 'NSW', 'Epping');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (30,'7219 Utah Parkway', 3011, 'VIC', 'Footscray');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (33,'71150 Boyd Park', 3012, 'VIC', 'Brooklyn');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (39,'248 8th Court', 3011, 'VIC', 'Footscray');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (39,'9 Bellgrove Point', 3013, 'VIC' ,'Yarraville');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (21,'54 Cardinal Pass', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (22,'0838 Spaight Circle', 4000, 'QLD', 'Brisbane');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (23,'58205 Melrose Center', 3000, 'VIC', 'Melbourne');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (40,'2 Stoughton Park', 4344, 'QLD', 'Egypt');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (41,'2 Sauthoff Drive', 4039, 'QLD', 'Marburg');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (42,'379 Iowa Street', 3016, 'VIC', 'Williamston');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (43,'36 Gale Center', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (44,'50639 Helena Street', 3023, 'VIC', 'Burnside');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (45,'549 Warrior Place', 3023, 'VIC', 'Burnside');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (87,'82587 Dapin Drive',  4039, 'QLD', 'Marburg');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (86,'2368 Ridgeview Alley', 2560, 'NSW', 'Airds');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (85,'46 Portage Road', 4352, 'QLD', 'Evergreen');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (84,'5076 Bay Court', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (83,'6 Heffernan Way', 3024, 'VIC', 'Mambourin');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (82,'468 Nova Plaza',  4039, 'QLD', 'Marburg');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (81,'354 Crest Line Hill', 3026, 'VIC', 'Laverton North');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (70,'6 Meadow Valley Plaza', 4005, 'QLD', 'New Farm');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (71,'5976 Rieder Junction', 4352, 'QLD', 'Evergreen');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (72,'56 Macpherson Junction', 3030, 'VIC', 'Derrimut');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (73,'985 Graceland Park', 4001, 'QLD', 'Fortitude Valley');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (75,'5947 Village Parkway', 3049, 'VIC', 'Attwood');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (76,'346 Crowley Place', 3360, 'VIC', 'Linton');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (77,'6560 Farmco Place', 3460, 'VIC', 'Basalt');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (69,'3885 Nelson Center', 3269, 'VIC','Princetown');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (68,'5 Thompson Road', 2120, 'NSW', 'Pennant Hills');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (67,'9730 Surrey Plaza', 4152, 'QLD', 'Carina');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (66,'25 Cottonwood Avenue', 2571, 'NSW', 'Picton');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (65,'85103 Melody Street', 2560, 'NSW', 'Airds');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (64,'61 Trailsway Hill', 4000, 'QLD', 'Brisbane');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (63,'029 Maple Crossing', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (62,'92425 Comanche Avenue', 4745, 'QLD', 'Norwich Place');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (59,'23 Lawn Pass', 4802, 'QLD', 'Airlie Beach');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (58,'149 Lillian Alley', 2127, 'NSW', 'Homebush Bay');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (57,'30 Judy Park', 2124, 'NSW', 'Parramatta');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (56,'53955 Nevada Place', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (55,'3849 Burning Wood Road', 4613, 'QLD', 'Coverty');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (54,'66 Carberry Park',  2423, 'NSW', 'Markwell');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (53,'838 Elmside Street', 4416, 'QLD', 'Nangram');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (52,'746 Swallow Place', 4800, 'QLD', 'Dingo Beach');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (98,'80 Sutteridge Center', 4101, 'QLD', 'Highgate Hill');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (99,'5066 Kingsford Plaza', 4478, 'QLD', 'Minnie Downs');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (64,'592 Kenwood Hill', 4390, 'QLD', 'Billa Billa');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (68,'92330 Amoth Drive', 4006, 'QLD', 'Bowen Hills');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (60,'1 Delaware Park', 4229, 'QLD', 'Bond University');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (50,'14 Esker Place', 2560, 'NSW', 'Airds');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (49,'2 Spaight Center', 4512, 'QLD', 'Wamuran');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (48,'9972 Brown Hill', 4350, 'QLD', 'Glenvale');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (47,'54 Bartillon Hill', 4347, 'QLD', 'Grantham');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (46,'516 Bayside Road', 4002, 'QLD', 'City East');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (44,'6787 Sherman Junction', 2135, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (44,'333 Pennsylvania Street', 2423, 'NSW', 'Markwell');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (45,'6 Barby Junction', 4454, 'QLD', 'Beilba');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (61,'3 Doe Crossing Pass', 2560, 'NSW', 'Airds');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (60,'0 Rutledge Alley', 2119, 'NSW', 'Beercroft');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (59,'03 Ludington Place', 4361, 'QLD', 'Back Plains');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (58,'49703 Buena Vista Place', 2136, 'NSW', 'Enfield');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (57,'260 Mandrake Hill', 4865, 'QLD', 'Gordonvale');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (56,'17109 Clemons Street', 4313, 'QLD', 'Biarra');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (55,'601 Commercial Drive', 3723, 'VIC', 'Archertown');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (54,'9989 Ruskin Junction', 4313, 'QLD', 'Biarra');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (53,'1 Service Park', 4880, 'QLD', 'Arriga');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (52,'7836 Hanover Junction', 3723, 'VIC', 'Archertown');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (51,'77731 Manufacturers Lane', 3723, 'VIC', 'Archertown');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (50,'2958 Warbler Place', 2000, 'NSW', 'Sydney');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (36,'646 Ilene Street', 2121, 'NSW', 'Epping');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (35,'99 Grasskamp Circle', 4580, 'QLD', 'Cooloola');
insert into customer_addresses (customer_id, address_line_1, post_code, state, suburb) values (33,'35 Coleman Street', 4550, 'QLD', 'Landsborough');



-- bank account types
insert into account_types (description) values ('Savings');
insert into account_types (description) values ('Cheque');
insert into account_types (description) values ('Loan');
insert into account_types (description) values ('Term Deposit');

-- interest
insert into interests (account_type_id, start_date, interest, description) values (1, now(), 1.3, 'INTEREST 1 AUGUST 2015');
insert into interests (account_type_id, start_date, interest, description) values (2, now(), 1.2, 'INTEREST 1 AUGUST 2015');
insert into interests (account_type_id, start_date, interest, description) values (3, now(), 1.5, 'INTEREST 1 AUGUST 2015');
insert into interests (account_type_id, start_date, interest, description) values (4, now(), 1.4, 'INTEREST 1 AUGUST 2015');



 
-- branches`
insert into branches (name, phone) values ('Brisbane', '(07)3194-4944');
insert into branches (name, phone) values ('Sydney', '(02)9222-9999');



insert into users (role, username, password, created, modified) values ('ADMIN'
, 'RMITAdmin','864890b5125b1e1990ecf1aed8c645d242339258', NOW(), NOW());

insert into users (role, username, password, created, modified, customer_id) values ('CUSTOMER'
, 'RMITCustomer','c373ca5eef84d405c45b17bfe4397d68602d42f9', NOW(), NOW(), 2);


-- accounts
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (1,1,'2015-01-01',1,2);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id)values (1,1,'2015-01-01',2,2);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (1,1,'2015-03-08',2,3);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (1,1,'2015-03-08',1,3);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (1,1,'2013-01-16',1,4);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (1,1,'2013-01-16',2,4);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (1,1,'2012-01-25',1,5);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (1,1,'2012-01-25',2,5);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (2, 1, '2015-03-03',1,7);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (2, 1, '2015-04-03',2,8);
insert into accounts (branch_id,  active, open_date, account_type_id, customer_id) values (2, 1, '2015-05-03',1,9);



-- transactiontypes

insert into transaction_types (description) values ('Credit'),('Debit');

-- transactions`

-- add some extra transactions for our CUSTOMER login 

insert into transactions (transaction_type_id, account_id, transaction_date, amount, description)
values 
(1, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -50 DAY), 4000.00, 'WORK PAY'),
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -44 DAY), 4.00, 'GROCERIES'),
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -43 DAY), 25.45, 'GROCERIES'),
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -42 DAY), 3.50, 'GROCERIES'),
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -40 DAY), 45.00, 'GROCERIES'),
 (1, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -30 DAY), 500.00, 'WORK PAY'),
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -20 DAY), 50.00, 'GROCERIES'),
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -20 DAY), 22.00, 'GROCERIES'),
 (1, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -14 DAY), 500.00, 'WORK PAY'),
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -10 DAY), 35.00, 'GROCERIES'), 
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -7 DAY), 55.00, 'GROCERIES'), 
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -5 DAY), 67.00, 'GROCERIES'), 
(2, (select min(id) from accounts where customer_id = 2), date_add(now(), INTERVAL -3 DAY), 4.00, 'GROCERIES');
 
-- update account descriptions
UPDATE accounts a
        INNER JOIN
    customers c ON c.id = a.customer_id
        INNER JOIN
    account_types t ON t.id = a.account_type_id 
SET 
    a.account_description = CONCAT(first_name,
            ' ',
            last_name,
            ' ',
            t.description);


-- set up the web user
CREATE USER 'webadmin'@'localhost' IDENTIFIED BY 'Sugar1*';
GRANT EXECUTE ON thisbank.* TO 'webadmin'@'localhost';
GRANT INSERT on thisbank.* TO 'webadmin'@'localhost';
GRANT SELECT on thisbank.* TO 'webadmin'@'localhost';
GRANT UPDATE on thisbank.* TO 'webadmin'@'localhost';
GRANT DELETE on thisbank.* TO 'webadmin'@'localhost';
FLUSH PRIVILEGES;


