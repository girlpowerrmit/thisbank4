Assignment Group:
	Lara Wilson: s3342496 (submitting on behalf of group)
	Christine Zhu: s3499001
	Melissa Thompson: s3338093
	
Best Point of Contact for this Assignment:
	Lara Wilson: s3342496@student.rmit.edu.au 

AWS URL: http://54.79.29.206/
		AWS Login for admin user: 
		Username:  RMITAdmin	
		Password:  this->Bank
		
		AWS Login for customer:
		Username:  RMITCustomer
		Password:  this->BankC

Trello Board: 
	We have invited Halil to our trello board. 
	We have also made the board public just before submission for other markers to access if necessary. 
	https://trello.com/b/ECL4dY4a/web-development-tech-assignment-2-group-php

Bitbucket: 
	HalilAli has been invited to our repository. 
	https://bitbucket.org/girlpowerrmit/thisbank4

Lamp Stack Set-up:
	Steps to Set up AWS Instance
	1. Registered for AWS Account
	2. Created EC2 Instance and Launch
	3. Installed Ubuntu Server 14.04
	4. Select t2.micro instance and launch
	5. Create Security Group and set inbound rules for:
		SSH -> Port 22: IP restriction with own IP (I have had to disable as I have a dynamic IP address, however I understand the security ramifications of limiting access to my server and acknowledge it accordingly)
		HTTP -> Port 80: Set source "anywhere".  
		HTTPS -> Port 443: Secure HTTP. 
	6. Launch Server
	7. Create new key pair. 
	7. Install LAMP software stack.
			Linux, Apache, MySql Php Software stack. 
	8. Create public and private keys
	9. Prepared document root.

Notes about our assignment.
	a) To test the different authorization settings, you can log in with the logins above to test on the AWS instance. 
	b) contacts/index.ctp -> The contact us page, does do the final mail out. Its for cosmetic purposes for the design feature. We didn't add mailer functionality.  
	c) To create and install the sql database, run the attached file dbcreate.sql
