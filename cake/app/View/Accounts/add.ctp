<!-- The accounts add View page. Formate table, echo form and display -->
<div class="accounts form">
	<?php
		echo $this->Form->create('Account',array(
		'class' => 'form-group',
		'inputDefaults' => array(
		'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
		'div' => array('class' => 'control-group'),
		'label' => array('class' => 'control-label'),
		'between' => '<div class="controls">',
		'after' => '</div>',
		'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
	)));
	?>

	<fieldset>
	<h2><?php echo __('Add Account'); ?></h2>
	<?php
		echo $this->Form->input('branch_id');
		echo $this->Form->input('account_description');
		echo $this->Form->input('active',array('checked' => 1,));
		echo $this->Form->input('account_type_id');
		echo $this->Form->input('customer_id',
		array('customer_id' => 'name','empty' => '(choose one)'));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
