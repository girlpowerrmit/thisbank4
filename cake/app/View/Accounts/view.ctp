<!-- Accounts View Page, this page is not in the navigation, but admin can still access directly through the URL -->
<div class="accounts view">
	<h2><?php echo __('View Account'); ?></h2>
		<dl>
			<dt><?php echo __('Account Number'); ?></dt>
			<dd><?php echo h($account['Account']['id']); ?></dd>
			<dt><?php echo __('Account Description'); ?></dt>
			<dd><?php echo h($account['Account']['account_description']); ?></dd>
			<dt><?php echo __('Open Date'); ?></dt>
			<dd><?php echo date('d-M-Y', strtotime(h($account['Account']['open_date']))); ?></dd>
			<dt><?php echo __('Search'); ?></dt>
			<dd><?php echo $this->Html->link("Search", array('controller' => 'transactions', 'action' => 'view', 
				$account['Account']['id'])); ?></dd>
			<dt><?php echo __('Fund Transfer'); ?></dt>
			<dd><?php echo $this->Html->link("Transfer", array('controller' => 'transactions', 'action' => 'add', 
				$account['Account']['id'])); ?></dd>
		</dl>
	<div class="related">
		<h3><?php echo __('Related Transactions'); ?></h3>
		<?php if (!empty($account['Transaction'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Transaction Date'); ?></th>
				<th><?php echo __('Debit'); ?></th>
				<th><?php echo __('Credit'); ?></th>
				<th><?php echo __('Description'); ?></th>
				<th><?php echo __('Destination Account'); ?></th>
				<th><?php echo __('Balance'); ?></th>
			</tr>
		<?php
			//foreach ($account['Transaction'] as $transaction):
			for($i=count($account['Transaction'])-1;$i>=0;$i--) {
			?>
			<tr>
				<td><?php echo date('d-m-Y',strtotime($account['Transaction'][$i]['transaction_date'])); ?></td>
				<td><?php 
					if($account['Transaction'][$i]['transaction_type_id']==1){ echo "";} 
					else{echo '$'.number_format($account['Transaction'][$i]['amount'],2);} ?>
				</td>
				<td><?php
					if($account['Transaction'][$i]['transaction_type_id']==1)
					{ echo '$'.number_format($account['Transaction'][$i]['amount'],2);}
					else {echo "";} ?>
				</td>
				<td><?php echo $account['Transaction'][$i]['description']; ?></td>
				<td><?php
					if(empty($account['Transaction'][$i]['DestinationAccount']['account_description'])){echo "";}
					else {echo $account['Transaction'][$i]['DestinationAccount']['account_description'];}?>
				</td>
				<td><?php
					if($i==count($account['Transaction'])-1) {echo '$'.  number_format($balance,2);}
					else if($account['Transaction'][$i+1]['transaction_type_id']==1){
						$balance-=$account['Transaction'][$i+1]['amount'];
						echo '$'.  number_format($balance,2);				}
					else {
						$balance+=$account['Transaction'][$i+1]['amount'];
						echo '$'.  number_format($balance,2);}?>
				</td>
			</tr>
		<?php } ?>
		</table>
		<?php endif; ?>
	</div>
</div>
