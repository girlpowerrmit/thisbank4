<!-- The Accounts Edit View Form. Style the table, output the table, and add submit button -->
<div class="accounts form">
	<h2><?php echo __('Edit Account'); ?></h2>
	<?php
		echo $this->Form->create('Account',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
	?>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('branch_id');
		echo $this->Form->input('account_description');
		echo $this->Form->input('active');
		echo $this->Form->input('account_type_id');
		echo $this->Form->input('customer_id');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
