<!-- Accounts Index Page. If admin logs in, display an accounts search form. Customer and -->
<!-- Admin see different options against each account also. -->
<div class="accounts index">
	<div class = "row">
		<?php
			if (AuthComponent::user('role') === 'ADMIN'){
				echo "<div class = 'col-md-4'>";
				echo "<div class='filters'>";
				echo "<h3>Search Accounts</h3>";
				$base_url = array('controller' => 'Accounts', 'action' => 'index');
				echo $this->Form->create('search',array(
					'url' => $base_url,
					'class' => 'form-group',
					'inputDefaults' => array(
					'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
				)));

				echo $this->Form->input("account_description", array('label' => 'Account','placeholder' => 'eg. Mary savings'));
				echo $this->Form->input("name", array('label' => 'Customer Name','placeholder' => 'eg. Mary Robertson'));
				echo $this->Form->input("branch", array('label' => 'Branch Name','placeholder' => 'eg. Brisbane'));
				echo $this->Form->input("account_type_desc", array('label' => 'Account Type', 'options' => array('1' => 'Savings', 
					'2' => 'Cheque', '' . '3' => 'Loan', '4' => 'Term Deposit') ,'empty' => '(Choose one)'));
				echo $this->Form->input('datestart', array('class' => 'searchDate', 'label' => 'Open From', 'type'=>'date', 
					'minYear' => 2001, 'maxYear' => date('Y'), 'dateFormat' => 'Y-M-D', 'selected' => '2001-01-01'));
				echo $this->Form->input('dateend', array('class' => 'searchDate', 'label' => 'Open To', 'type'=>'date', 
					'minYear' => 2001, 'maxYear' => date('Y'), 'dateFormat' => 'Y-M-D',  'selected' =>date('Y-m-d')));
				echo $this->Form->input('interest', array('label' => 'Interest Rate', 'type' => 'number', 'min' => '0.01',
					'max' => '2500', 'step' => '0.01'));
				echo $this->Form->submit("Search");

				// To reset all the filters we only need to redirect to the base_url
				echo "<div class='submit actions'>";
				echo $this->Html->link("Reset",$base_url);
				echo "</div>";
				echo $this->Form->end();
				echo "</div>";
				echo "</div>";
			}

		?>
		<div class = "col-md-8">
		<?php 
			$role = AuthComponent::user('role');
			if ($role === 'ADMIN') {
				echo "<br/>";
				echo $this->Html->link('Add Account', array('controller' => 'accounts', 'action' => 'add'),
                        	array('bootstrap-type' => 'primary', 'class' => 'btn btn-primary', 'rule' => 'button'));
				}
		?>
		<h2><?php echo __('Accounts'); ?></h2>
		<table class="table table-striped table-hover">
			<tr>
				<th><?php echo $this->Paginator->sort('id'); ?></th>
				<th><?php echo $this->Paginator->sort('branch_id'); ?></th>
				<th><?php echo $this->Paginator->sort('account_description'); ?></th>
				<th><?php echo $this->Paginator->sort('active'); ?></th>
				<th><?php echo $this->Paginator->sort('open_date'); ?></th>
				<th><?php echo $this->Paginator->sort('account_type_id'); ?></th>
				<th><?php echo 'Balance' ; ?> </th>
				<?php
					$role = AuthComponent::user('role');
					if ($role === 'ADMIN'){
						echo "<th>";
						echo $this->Paginator->sort('customer_id');
						echo "</th>";
					}
				?>

				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php foreach ($accounts as $account): ?>
			<tr>
				<td><?php echo h($account['Account']['id']); ?>&nbsp;</td>
				<td><?php echo h($account['Branch']['name']); ?> &nbsp;</td>
				<td><?php echo h($account['Account']['account_description']); ?>&nbsp;</td>
				<td>
				<?php
					if(h($account['Account']['active']) == 1) {echo 'Yes';}
					else {echo 'No';}
				?>
				</td>
				<td> <?php echo date("d-M-Y",strtotime(h($account['Account']['open_date']))); ?> </td>
				<td><?php echo h($account['AccountType']['description']); ?>&nbsp;</td>
				<td>
				<?php
					foreach($CurrentBalance as $id => $balance) {
						if($id == $account['Account']['id']) {echo '$' . number_format($balance,2);}
					}
				?>
				</td>
				<?php
					$role = AuthComponent::user('role');
					if ($role === 'ADMIN'){
					echo "<td>";
					echo $this->Html->link($account['Customer']['name'], array('controller' => 'customers', 'action' => 'edit', 
						$account['Customer']['id']));}
					echo "</td>";
				?>
				<td class="actions">
				<?php
					$role = AuthComponent::user('role');
					if ($role === 'CUSTOMER') {
						echo $this->Html->link(__('Search Transactions '), array('controller' => 'transactions', 'action' => 'view', 
							$account['Account']['id']));
						echo "<br/>";
						echo $this->Html->link(__('Transfer Money '), array('controller' => 'transactions', 'action' => 'add', 
							$account['Account']['id']));
					}
					if ($role === 'ADMIN') {
						echo $this->Html->link(__('Edit Account'), array('action' => 'edit', $account['Account']['id']));
						echo "</br>";
					}
				?>
				</td>
			</tr>

		<?php endforeach; ?>
		</table>
		<div class="pagination pagination-sm">
			<ul class="pagination">
			<?php
				echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
				echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, 
					array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			?>
			</ul>
		</div>
		<p>
		<?php
			echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} 
				total, starting on record {:start}, ending on {:end}')
			));
		?>      
		</p>
	</div>
</div>
