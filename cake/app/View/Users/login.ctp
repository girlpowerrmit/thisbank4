<!-- Users login view form. -->
<div class="users form">
	<h2><?php echo __('User Login'); ?></h2>
	<?php 
		echo $this->Session->flash('auth'); ?>
	<?php
	echo $this->Form->create('User',array(
		'class' => 'form-group',
		'inputDefaults' => array(
		'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
		'div' => array('class' => 'control-group'),
		'label' => array('class' => 'control-label'),
		'between' => '<div class="controls">',
		'after' => '</div>',
		'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
	)));
	?>
	<fieldset>
		<legend>
			<?php echo __('Please enter your username and password'); ?>
		</legend>

	<?php
		echo $this->Form->input('username');
		echo $this->Form->input('password');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Login')); ?>
</div>
