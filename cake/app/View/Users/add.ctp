<!-- Add Users View Form - Prettys up display and outputs fields to add a new user -->
<div class="users form">
	<h2><?php echo __('Add User'); ?></h2>
	<?php
		echo $this->Form->create('User',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)))
	?>
	

	<fieldset>
		
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('role', array(
		'options' => array('ADMIN' => 'Admin', 'CUSTOMER' => 'Customer'),
		'empty' => '(choose one)'
	));

		echo $this->Form->input('customer_id', array('customer_id' => 'name', 'empty' => '(choose one)'));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
