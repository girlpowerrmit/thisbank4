<!-- Users index page. Accessible only by admin. Search form to search users. -->
<div class="users index">
	<div class = "row">
		<div class = "col-md-4">
			<div class="filters">
			<h3>Search Users</h3>
			<?php
				$base_url = array('controller' => 'Users', 'action' => 'index');
				echo $this->Form->create('search',array(
					'url' => $base_url,
					'class' => 'form-group',
					'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
				)));
				echo $this->Form->input('username', array('label' => 'Username', 'placeholder' => 'eg. adminUser'));
				echo $this->Form->input('name', array('type' => 'text', 'placeholder' => 'eg. Mary Robertson', 
					'label' => 'Customer'));
				echo $this->Form->input('role', array('options' => array('ADMIN' => 'Admin', 'CUSTOMER' => 
					'Customer'), 'empty' => '(choose one)'));
				echo $this->Form->input('datestart', array('class' => 'searchDate', 'label' => 'Create From', 
					'type'=>'date', 'minYear' => 2014, 'maxYear' => date('Y'), 'dateFormat' => 'Y-M-D', 'selected' => '2014-01-01'));
				echo $this->Form->input('dateend', array('class' => 'searchDate', 'label' => 'Create To', 'type'=>'date', 
					'minYear' => 2014, 'maxYear' => date('Y'), 'dateFormat' => 'Y-M-D',  'selected' =>date('Y-m-d')));

				// Search!
				echo $this->Form->submit("Search");

				// To reset all the filters we only need to redirect to the base_url
				echo "<div class='submit actions'>";
				echo $this->Html->link("Reset",$base_url);
				echo "</div>";
				echo $this->Form->end();
			?>
			</div>
		</div>
		<div class = "col-md-8">
		<br/>
		<?php echo $this->Html->link('Add User', array('controller' => 'users', 'action' => 'add'), 
    			array('bootstrap-type' => 'primary', 'class' => 'btn btn-primary', 'rule' => 'button'));?>
		<h2><?php echo __('Users'); ?></h2>
		<table class="table table-striped table-hover">
			<tr>
				<th><?php echo $this->Paginator->sort('id'); ?></th>
				<th><?php echo $this->Paginator->sort('role'); ?></th>
				<th><?php echo $this->Paginator->sort('username'); ?></th>
				<th><?php echo $this->Paginator->sort('created'); ?></th>
				<th><?php echo $this->Paginator->sort('modified'); ?></th>
				<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['role']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
				<td><?php echo date("d-m-Y H:i:s",strtotime(h($user['User']['created']))); ?>&nbsp;</td>
				<td><?php echo date("d-m-Y H:i:s",strtotime(h($user['User']['modified']))); ?>&nbsp;</td>
				<td><?php echo h($user['Customer']['name']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
                                </td>
			</tr>
			<?php endforeach; ?>
		</table>
		<div class="pagination pagination-sm">
			<ul class="pagination">
			<?php
				echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
				echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			?>
			</ul>
		</div>
		<p>
		<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));
		?>      
		</p>
	</div>
</div>
