<!-- Users view page not available in navigation, but accessible directly in URL only by admin -->
<div class="users view">
	<h2><?php echo __('View User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd><?php echo h($user['User']['id']); ?></dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd><?php echo h($user['User']['role']); ?></dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd><?php echo h($user['User']['username']); ?></dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd><?php echo h($user['User']['password']); ?></dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd><?php echo h($user['User']['created']); ?></dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd><?php echo h($user['User']['modified']); ?></dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd><?php echo $this->Html->link($user['Customer']['id'], array('controller' => 'customers', 
			'action' => 'view', $user['Customer']['id'])); ?></dd>
	</dl>
</div>
