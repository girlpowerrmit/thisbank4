<!-- Accounts View Page. Not in the navigation but can be accessed by admin directly in the URL -->
<div class="accountTypes view">
	<h2><?php echo __('View Account Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd><?php echo h($accountType['AccountType']['id']); ?></dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>?php echo h($accountType['AccountType']['description']); ?></dd>
	</dl>
	
	<div class="related">
		<h3><?php echo __('Related Accounts'); ?></h3>
		<?php if (!empty($accountType['Account'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Branch Id'); ?></th>
				<th><?php echo __('Active'); ?></th>
				<th><?php echo __('Open Date'); ?></th>
				<th><?php echo __('Account Type Id'); ?></th>
				<th><?php echo __('Customer Id'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php
			$i = 0;
			foreach ($accountType['Account'] as $account): ?>
				<tr>
					<td><?php echo $account['id']; ?></td>
					<td><?php echo $account['branch_id']; ?></td>
					<td><?php echo $account['active']; ?></td>
					<td><?php echo $account['open_date']; ?></td>
					<td><?php echo $account['account_type_id']; ?></td>
					<td><?php echo $account['customer_id']; ?></td>
					<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'accounts', 'action' => 'view', $account['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'accounts', 'action' => 'edit', $account['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accounts', 'action' => 'delete', $account['id']), 
						null, __('Are you sure you want to delete # %s?', $account['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<?php endif; ?>

		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>
	
	<div class="related">
		<h3><?php echo __('Related Interests'); ?></h3>
		<?php if (!empty($accountType['Interest'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Account Type Id'); ?></th>
				<th><?php echo __('Start Date'); ?></th>
				<th><?php echo __('End Date'); ?></th>
				<th><?php echo __('Interest'); ?></th>
				<th><?php echo __('Description'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
		<?php
		$i = 0;
		foreach ($accountType['Interest'] as $interest): ?>
			<tr>
				<td><?php echo $interest['id']; ?></td>
				<td><?php echo $interest['account_type_id']; ?></td>
				<td><?php echo $interest['start_date']; ?></td>
				<td><?php echo $interest['end_date']; ?></td>
				<td><?php echo $interest['interest']; ?></td>
				<td><?php echo $interest['description']; ?></td>
				<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'interests', 'action' => 'view', $interest['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'interests', 'action' => 'edit', $interest['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'interests', 'action' => 'delete', $interest['id']), null, __('Are you sure you want to delete # %s?', $interest['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
		<?php endif; ?>

		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Interest'), array('controller' => 'interests', 'action' => 'add')); ?> </li>
			</ul>
		</div>
</div>
