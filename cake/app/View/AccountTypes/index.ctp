<!-- Account types index view, Display the account types available -->
<div class="accountTypes index">
	<br/>
	<?php echo $this->Html->link('Add Account Type', array('controller' => 'accountTypes', 'action' => 'add'),
                        array('bootstrap-type' => 'primary', 'class' => 'btn btn-primary', 'rule' => 'button'));?>
	<h2><?php echo __('Account Types'); ?></h2>
	<table style = "width:50%" class="table table-striped table-hover">
		<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('description'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
			<?php foreach ($accountTypes as $accountType): ?>
			<tr>
				<td><?php echo h($accountType['AccountType']['id']); ?>&nbsp;</td>
				<td><?php echo h($accountType['AccountType']['description']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit',
						 $accountType['AccountType']['id'])); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div class="pagination pagination-sm">
		<ul class="pagination">
		<?php
			echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, 
				array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 
				'currentClass' => 'active','tag' => 'li','first' => 1));
			echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), 
				null, array('tag' => 'li', 'class' => 'disabled','disabledTag' => 'a'));
		?>
		</ul>
	</div>
	<p>
	<?php
		echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} 
			records out of {:count} total, starting on record {:start}, ending on {:end}')));
	?>      
	</p>
</div>
