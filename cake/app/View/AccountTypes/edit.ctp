<!-- AccountTypes Edit page. Display the form with prefilled data -->
<div class="accountTypes form">
	<h2><?php echo __('Edit Account Type'); ?></h2>
	<?php
		echo $this->Form->create('AccountType',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
		?>
	<fieldset>
		<legend><?php echo __('Edit Account Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('description');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
