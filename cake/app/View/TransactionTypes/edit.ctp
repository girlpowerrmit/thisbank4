<!-- Transaction Types Edit View -> Prepopulates data into the fields to be edited for this transaction type -->
<div class="transactionTypes form">
	<h2><?php echo __('Edit Transaction Type'); ?></h2>
	<?php
		echo $this->Form->create('TransactionType',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));

	?>
	<fieldset>
		<legend><?php echo __('Edit Transaction Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('description');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
