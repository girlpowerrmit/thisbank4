<!-- TransactionsTypes View, not available in navigation but is accessible by admin in the URL directly. -->
<div class="transactionTypes view">
	<h2><?php echo __('View Transaction Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd><?php echo h($transactionType['TransactionType']['id']); ?></dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd><?php echo h($transactionType['TransactionType']['description']); ?></dd>
	</dl>

	<div class="related">
		<h3><?php echo __('Related Transactions'); ?></h3>
		<?php if (!empty($transactionType['Transaction'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Transaction Type Id'); ?></th>
				<th><?php echo __('Account Id'); ?></th>
				<th><?php echo __('Transaction Date'); ?></th>
				<th><?php echo __('Amount'); ?></th>
				<th><?php echo __('Description'); ?></th>
				<th><?php echo __('Destination Account'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php
			$i = 0;
			foreach ($transactionType['Transaction'] as $transaction): ?>
			<tr>
				<td><?php echo $transaction['id']; ?></td>
				<td><?php echo $transaction['transaction_type_id']; ?></td>
				<td><?php echo $transaction['account_id']; ?></td>
				<td><?php echo $transaction['transaction_date']; ?></td>
				<td><?php echo $transaction['amount']; ?></td>
				<td><?php echo $transaction['description']; ?></td>
				<td><?php echo $transaction['destination_account']; ?></td>
				<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'transactions', 'action' => 'view', $transaction['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'transactions', 'action' => 'edit', $transaction['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'transactions', 'action' => 'delete', $transaction['id']), null, __('Are you sure you want to delete # %s?', $transaction['id'])); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
		<?php endif; ?>
	</div>
</div>
