<!-- Add Transaction Form -->
<div class="transactionTypes form">
	<h2><?php echo __('Add Transaction Type'); ?></h2>
	<?php
	echo $this->Form->create('TransactionType',array(
		'class' => 'form-group',
		'inputDefaults' => array(
		'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
		'div' => array('class' => 'control-group'),
		'label' => array('class' => 'control-label'),
		'between' => '<div class="controls">',
		'after' => '</div>',
		'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
	)));

	?>
	<fieldset>
	<?php
		echo $this->Form->input('description');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>

