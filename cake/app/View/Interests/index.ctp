<!-- Index Page for Interests View. Display all interests currently in the database -->
<div class="interests index">
	<br/>
	<?php echo $this->Html->link('Add Interest', array('controller' => 'interests', 'action' => 'add'),
                        array('bootstrap-type' => 'primary', 'class' => 'btn btn-primary', 'rule' => 'button'));?>
	<h2><?php echo __('Interests'); ?></h2>
	<table class="table table-striped table-hover">
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('end_date'); ?></th>
			<th><?php echo $this->Paginator->sort('interest'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		<?php foreach ($interests as $interest): ?>
		<tr>
			<td><?php echo h($interest['Interest']['id']); ?>&nbsp;</td>
			<td><?php echo h($interest['AccountType']['description']); ?></td>
			<td><?php echo h(date("d/m/Y",  strtotime($interest['Interest']['start_date']))); ?>&nbsp;</td>
			<td><?php
				if (!empty($interest['Interest']['end_date'])) {
					echo h(date("d/m/Y",strtotime($interest['Interest']['end_date'])));}
				else {echo "";}?> </td>
			<td><?php echo h($interest['Interest']['interest']); ?>&nbsp;</td>
			<td><?php echo h($interest['Interest']['description']); ?>&nbsp;</td>
			<td class="actions"> 
				<?php echo $this->Html->link(__('Edit'), array('action' => 'edit',
					$interest['Interest']['id'])); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div class="pagination pagination-sm">
		<ul class="pagination">
		<?php
			echo $this->Paginator->prev(__('prev'), array('tag' => 'li'),
			null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a',
			'currentClass' => 'active','tag' => 'li','first' => 1));
			echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'),
			null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		?>
		</ul>
	</div>
	<p>
	<?php
	 echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records'
		. ' out of {:count} total, starting on record {:start}, ending on {:end}')));
	?>      
	</p>
</div>
