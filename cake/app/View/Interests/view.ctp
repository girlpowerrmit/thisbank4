<!-- Interests View Page, not in navigation but can be accessed directly via the URL by administration -->
<div class="interests view">
	<h2><?php echo __('View Interest'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd><?php echo h($interest['Interest']['id']); ?></dd>
		<dt><?php echo __('Account Type'); ?></dt>
		<dd><?php echo $this->Html->link($interest['AccountType']['id'], array('controller' => 
			'account_types', 'action' => 'view', $interest['AccountType']['id'])); ?></dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd><?php echo h($interest['Interest']['start_date']); ?></dd>
		<dt><?php echo __('End Date'); ?></dt>
		<dd><?php echo h($interest['Interest']['end_date']); ?></dd>
		<dt><?php echo __('Interest'); ?></dt>
		<dd><?php echo h($interest['Interest']['interest']); ?></dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd><?php echo h($interest['Interest']['description']); ?></dd>
	</dl>
</div>

