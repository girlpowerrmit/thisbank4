<!-- Add a new interest item -->
<div class="interests form">
	<h2><?php echo __('Add Interests'); ?></h2>
	<?php
		echo $this->Form->create('Interest',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
	?>
	<fieldset>
		
	<?php
		echo $this->Form->input('account_type_id');
		echo $this->Form->input('start_date',array('class' => 'searchDate', 'type'=>'date'));
		echo $this->Form->input('interest');
		echo $this->Form->input('description');
	?>
	</fieldset>
	<?php 
		echo $this->Form->end(__('Submit')); ?>
</div>
