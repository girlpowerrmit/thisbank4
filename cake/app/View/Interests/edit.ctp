<!-- Interests edit page. Auto fills information from the original interest to be edited -->
<div class="interests form">
	<h2><?php echo __('Edit Interest'); ?></h2>
	<?php
		echo $this->Form->create('Interest',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
			)));
	?>
	<fieldset>
		<legend><?php echo __('Edit Interest'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('account_type_id');
		echo $this->Form->input('start_date', array('class' => 'searchDate'));
		echo $this->Form->input('deactivate', array('checked' => 0,));
		echo $this->Form->input('interest');
		echo $this->Form->input('description');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
