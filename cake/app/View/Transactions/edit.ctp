<!-- Edit transactions, this is not allowed. -->
<div class="transactions form">
<?php echo $this->Form->create('Transaction'); ?>
	<fieldset>
		<legend><?php echo __('Edit Transaction'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('transaction_type_id');
		echo $this->Form->input('account_id');
		echo $this->Form->input('transaction_date');
		echo $this->Form->input('amount');
		echo $this->Form->input('description');
		echo $this->Form->input('destination_account');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
