<!-- The transactions view page displays a search form, and all of the transactions for a particular account -->

<div class="transactions view">
	<div class = "row">
		<div class = "col-md-4">
			<?php
			$options= array(
			'label' => 'Search',
			'id'=>'submit',
			'div' => FALSE,
			'onclick'=>'return checkForm()'
			);

			echo "<h3>Search Transactions</h3>";
			echo $this->Form->create('Transaction',array(
				'class' => 'form-group',
				'inputDefaults' => array(
				'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
				'div' => array('class' => 'control-group'),
				'label' => array('class' => 'control-label'),
				'between' => '<div class="controls">',
				'after' => '</div>',
				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline'))),
				'url'=>array('controller'=>'Transactions','action'=>'view',$accountId)));

			echo $this->Form->input('description');
			echo $this->Form->input('start_date', array('class' => 'searchDate', 'label'=>'Start Date', 'type'=>'date','selected'=>date('Y-m-d',strtotime('last month'))));
			echo $this->Form->input('end_date', array('class' => 'searchDate', 'label'=>'End Date','type'=>'date'));
			echo "<p> <b>Amount</b> </p>";
			echo $this->Form->input('from', array('id'=>'from', 'class'=>'amount'));
			echo $this->Form->input('to', array('id'=>'to', 'class'=>'amount'));
			echo $this->Form->input('transaction_type_id', array('options'=>array('1'=>'CREDIT','2'=>'DEBIT'), 'empty'=>""));
			echo $this->Form->input('currentBalance',array('type'=>'hidden', 'id'=>'currentBalance'));
			echo $this->Form->submit('Clear Form', array('name'=>'clear'));
			echo $this->Form->end($options);
			?>
		</div>
		
		<div class = "col-md-8">
			<h2><?php echo __('View Transaction'); ?></h2>
			<table class="table table-striped table-hover">
				<tr>
					<th><?php echo __('Id'); ?></th>
					<th><?php echo __('Date'); ?></th>
					<th><?php echo __('Description'); ?></th>
					<th><?php echo __('Debit'); ?></th>
					<th><?php echo __('Credit'); ?></th>
					<th><?php echo __('Destination Account'); ?></th>
					<?php if (empty($to) && empty($transactionType)) {
						echo "<th>".__('Balance')."</th>";}?>
				</tr>

				<?php foreach ($transactions as $transaction): ?>
				<tr>
					<td> <?php echo $transaction['Transaction']['id'] ;?> </td>
					<td> <?php echo date("d-M-Y", strtotime($transaction['Transaction']['transaction_date']));?> </td>
					<td> <?php echo $transaction['Transaction']['description'];?> </td>
					<?php
					if($transaction['TransactionType']['id']==1){
						echo "<td></td>";
						echo "<td> $".number_format($transaction['Transaction']['amount'],2). "</td>";
						echo "<td>".$transaction['DestinationAccount']['account_description']. "</td>";
						if(empty($to) && empty($transactionType)){
							echo "<td> $".number_format($CurrentBalance[$transaction['Transaction']['id']],2). "</td>";}
					}
					else{
						echo "<td> $".number_format($transaction['Transaction']['amount'],2). "</td>";
						echo "<td></td>";
						echo "<td>".$transaction['DestinationAccount']['account_description']. "</td>";
						if(empty($to) && empty($transactionType)){
							echo "<td> $".number_format($CurrentBalance[$transaction['Transaction']['id']],2). "</td>";}
					}
					?>
				</tr>

				<?php endforeach; ?>
				<label id='balance' type='hidden' value=$balance/>
			</table>

			<div class="pagination pagination-sm">
				<ul class="pagination">
				<?php
					echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
					echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));?>
				</ul>
			</div>
			<p>
			<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')	));
			?>      
			</p>
		</div>
	</div>
</div>

<script type="text/javascript">
function checkForm() {

var from=document.getElementById('from').value;
var to=document.getElementById('to').value;

if(isNaN(from) || isNaN(to) || from<0 || to<0)
{
alert("Must enter a valid positive amount");
return false;
}

else
if(from-to>0)
{
alert("The 'To Amount' must be greater than or equal to the 'From Amount'");
return false;
}
else
{

return true;
}
}
</script>
