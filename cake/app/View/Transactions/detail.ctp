<!-- Detail view, not accessible by navigation. Only accessible by admin directly in the URL -->
<div class="transactions view">
	<h2><?php echo __('Transaction Details'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd><?php echo h($transaction['Transaction']['id']); ?></dd>
		<dt><?php echo __('Transaction Type'); ?></dt>
		<dd><?php echo $this->Html->link($transaction['TransactionType']['id'], array('controller' => 'transaction_types', 
			'action' => 'view', $transaction['TransactionType']['id'])); ?></dd>
		<dt><?php echo __('Account'); ?></dt>
		<dd><?php echo $this->Html->link($transaction['Account']['id'], array('controller' => 'accounts', 'action' => 
			'view', $transaction['Account']['id'])); ?></dd>
		<dt><?php echo __('Transaction Date'); ?></dt>
		<dd><?php echo h($transaction['Transaction']['transaction_date']); ?></dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd><?php echo h($transaction['Transaction']['amount']); ?></dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd><?php echo h($transaction['Transaction']['description']); ?></dd>
		<dt><?php echo __('Destination Account'); ?></dt>
		<dd><?php echo h($transaction['Transaction']['destination_account']); ?></dd>
	</dl>
</div>
