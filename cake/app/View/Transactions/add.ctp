<!-- Add transactions is used for the Transfer Funds page between a clients accounts -->
<div class="transactions form">
	<h2><?php echo __('Transfer Funds'); ?></h2>
	<?php
		echo $this->Form->create('Transaction',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
	?>
	<br/>
	<fieldset>
	<?php
		$options= array(
		'label' => 'Submit',
		'id'=>'submit',
		'div' => FALSE,
		'onclick'=>'return addTrans()');
		echo 'Today is:       '.date('d-m-Y');
		echo '<br/><br/>';
		echo $this->Form->hidden('transaction_type_id', array("value"=>2, "name"=>'data[Transaction][transaction_type_id]'));
		echo $this->Form->hidden('id',array('value'=>$account['Account']['id'],'name'=>'data[Transaction][account_id]'));
		echo $this->Form->label($account['Account']['account_description']);
		echo "<br/><br/>";
		echo $this->Form->input('Current Balance', array('id'=>'balance', 'default'=>$balance, "disabled"=>'disabled'));
		echo "<br/>";
		echo $this->Form->input('amount', array('id'=>'amount', 'name'=>'data[Transaction][amount]'));
		echo $this->Form->input('description', array('id'=>'description', 'name'=>'data[Transaction][description]'));
		echo $this->Form->input('destination_account', array('options'=>$branches,'empty'=>'Select an account'));
	?>
	</fieldset>
	<?php echo $this->Form->end($options); ?>
</div>

<script type="text/javascript">
function addTrans() {

var amount=document.getElementById('amount').value;
var description=document.getElementById('description').value;
var balance=document.getElementById('balance').value;
if(isNaN(amount) || amount<0)
{
alert('Please enter a positive amount');
return false;
}
if (amount-balance>0)
{
alert('Your balance is lower than transactional amount');
return false;
}
if(description==0 )
{
alert('Please enter your description');
return false;
}
else
{
return true;
}
}
</script>
