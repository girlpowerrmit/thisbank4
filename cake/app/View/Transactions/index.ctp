<!-- Index page for transactions. Lists all transactions. If its a client their data is filtered before the data -->
<!-- is rendered -->
<div class="transactions index">
	<h2><?php echo __('Transactions'); ?></h2>
	<?php
		echo "<div class = 'col-md-4'>";
		echo "<div class='filters'>";
		$base_url = array('controller' => 'Transactions', 'action' => 'index');
		echo $this->Form->create('search',array(
			'url' => $base_url,
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before','label','between','input' =>
			array('class' => 'form-group'),'error','after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' =>
			array('wrap' => 'span', 'class' => 'help-inline')),
		)));
		echo $this->Form->input('options', array('label' => 'Accounts'));
		echo $this->Form->submit('Search');
		echo $this->Form->end();
		echo '</div>';
		echo '</div>';
	?>

	<table class="table table-striped table-hover">
		<tr>
			<th><?php echo $this->Paginator->sort('transaction_type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('account_id'); ?></th>
			<th><?php echo $this->Paginator->sort('transaction_date'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('destination_account'); ?></th>
		</tr>
		<?php foreach ($transactions as $transaction): ?>
		<tr>
			<td><?php echo h($transaction['TransactionType']['description']); ?>&nbsp;</td>
			<td><?php echo h($transaction['Account']['account_description']); ?></td>
			<td><?php echo date('d-m-Y h:i:s', strtotime(h($transaction['Transaction']['transaction_date']))); ?>&nbsp;</td>
			<td><?php echo '$' . number_format(h($transaction['Transaction']['amount']), 2); ?>&nbsp;</td>
			<td><?php echo h($transaction['Transaction']['description']); ?>&nbsp;</td>
			<td><?php echo h($transaction['Transaction']['destination_account']); ?>&nbsp;</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div class="pagination pagination-sm">
		<ul class="pagination">
		<?php
			echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
			echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			?>
		</ul>
	</div>
	<p>
	<?php
		echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of 	{:count} total, starting on record {:start}, ending on {:end}')));
	?>
	</p>
</div>
