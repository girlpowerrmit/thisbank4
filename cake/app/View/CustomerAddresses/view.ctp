<!-- Customer Address View Page, not available in navigation but can be accessed directly in URL by administrator -->
<div class="customerAddresses view">
	<h2><?php echo __('View Customer Address'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd><?php echo h($customerAddress['CustomerAddress']['id']); ?></dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd><?php echo $this->Html->link($customerAddress['Customer']['id'], array('controller' => 'customers', 
			'action' => 'view', $customerAddress['Customer']['id'])); ?></dd>
		<dt><?php echo __('Address Line 1'); ?></dt>
		<dd><?php echo h($customerAddress['CustomerAddress']['address_line_1']); ?></dd>
		<dt><?php echo __('Suburb'); ?></dt>
		<dd><?php echo h($customerAddress['CustomerAddress']['suburb']); ?></dd>
		<dt><?php echo __('Post Code'); ?></dt>
		<dd><?php echo h($customerAddress['CustomerAddress']['post_code']); ?></dd>
		<dt><?php echo __('State'); ?></dt>
		<dd><?php echo h($customerAddress['CustomerAddress']['state']); ?></dd>
	</dl>
</div>
