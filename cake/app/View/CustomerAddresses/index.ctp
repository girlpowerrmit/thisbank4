<!-- Customer Addresses Index page - Displays all of the customer addresses in a table -->
<div class="customerAddresses index">
	<h2><?php echo __('Customer Addresses'); ?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('address_line_1'); ?></th>
			<th><?php echo $this->Paginator->sort('suburb'); ?></th>
			<th><?php echo $this->Paginator->sort('post_code'); ?></th>
			<th><?php echo $this->Paginator->sort('state'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		<?php foreach ($customerAddresses as $customerAddress): ?>
		<tr>
			<td><?php echo h($customerAddress['CustomerAddress']['id']); ?>&nbsp;</td>
			<td> 
			<?php echo $this->Html->link($customerAddress['Customer']['id'], array('controller' => 'customers', 'action' => 'view', 		
				$customerAddress['Customer']['id'])); ?>
			</td>
			<td><?php echo h($customerAddress['CustomerAddress']['address_line_1']); ?>&nbsp;</td>
			<td><?php echo h($customerAddress['CustomerAddress']['suburb']); ?>&nbsp;</td>
			<td><?php echo h($customerAddress['CustomerAddress']['post_code']); ?>&nbsp;</td>
			<td><?php echo h($customerAddress['CustomerAddress']['state']); ?>&nbsp;</td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('action' => 'view', $customerAddress['CustomerAddress']['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $customerAddress['CustomerAddress']['id']), null, __('Are you sure you want to delete # %s?', $customerAddress['CustomerAddress']['id'])); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<p>
	<?php
		echo $this->Paginator->counter(array( 'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, 
			starting on record {:start}, ending on {:end}')));
		?>      
	</p>
	<div class="paging">
		<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
			<li><?php echo $this->Html->link(__('New Customer Address'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
