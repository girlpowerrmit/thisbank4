<!-- Customer Addresses Add Form, format and display fields to add a new Customer Address -->
<div class="customerAddresses form">
	<h2><?php echo __('Add Customer Address'); ?></h2>

	<?php
		echo "<br/>";
		echo $this->Form->create('CustomerAddress',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
		?>
	<fieldset>
	<?php
		echo $this->Form->input('customer_id', array('selected' => $selectedCustID, 'disabled' => true));
		echo $this->Form->input('address_line_1');
		echo $this->Form->input('suburb');
		echo $this->Form->input('post_code');
		echo $this->Form->input('state', array('options'=>array('VIC' => 'VIC','NSW'=>'NSW',
		'QLD'=>'QLD','WA'=>'WA','NT'=> 'NT','ACT'=>'ACT', 'SA' => 'SA', 'TAS' => 'TAS'),'State'));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
