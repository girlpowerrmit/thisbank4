<!-- Branches add View, display the form for the administrator to add a new branch -->
<div class="branches form">
<h2><?php echo __('Add Branch'); ?></h2>
	<?php
		echo $this->Form->create('Branch',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
			)));
		?>
	<fieldset>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('phone', array('id'=>'branchphone'));
	?>
	</fieldset>
		<?php echo $this->Form->end(__('Submit')); ?>
</div>
