<!-- Branches View Page, not available in navigation but admin can access directly via URL if they wish -->
<!-- Displays all related accounts for this branch that you are viewing -->
<div class="branches view">
	<h2><?php echo __('View Branch'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd><?php echo h($branch['Branch']['id']); ?></dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd><?php echo h($branch['Branch']['name']); ?></dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd><?php echo h($branch['Branch']['phone']); ?></dd>
	</dl>

	<div class="related">
		<h3><?php echo __('Related Accounts'); ?></h3>
		<?php if (!empty($branch['Account'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Branch Id'); ?></th>
				<th><?php echo __('Active'); ?></th>
				<th><?php echo __('Open Date'); ?></th>
				<th><?php echo __('Account Type Id'); ?></th>
				<th><?php echo __('Customer Id'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
		<?php
		$i = 0;
		foreach ($branch['Account'] as $account): ?>
			<tr>
				<td><?php echo $account['id']; ?></td>
				<td><?php echo $account['branch_id']; ?></td>
				<td><?php echo $account['active']; ?></td>
				<td><?php echo $account['open_date']; ?></td>
				<td><?php echo $account['account_type_id']; ?></td>
				<td><?php echo $account['customer_id']; ?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'accounts', 'action' => 'view', $account['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'accounts', 'action' => 'edit', $account['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accounts', 'action' => 'delete', $account['id']), 
						null, __('Are you sure you want to delete # %s?', $account['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
		<?php endif; ?>
	</div>
</div>
