<!-- Branches edit form. Format and display the form. Fields are populated with data from edited branch -->
<div class="branches form">
	<h2><?php echo __('Edit Branch'); ?></h2>
	<?php
		echo $this->Form->create('Branch',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
	?>
	<fieldset>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('phone', array('id'=>'phone'));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
