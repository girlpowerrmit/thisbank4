<!-- Branches index view, display a list of the branches currently with this bank -->
<div class="branches index">
	<br/>
	<?php echo $this->Html->link('Add Branch', array('controller' => 'branches', 'action' => 'add'),
                        array('bootstrap-type' => 'primary', 'class' => 'btn btn-primary', 'rule' => 'button'));?>
	<h2><?php echo __('Branches'); ?></h2>
	<table style = "width:50%" class="table table-striped table-hover">
		<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('phone'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		<?php foreach ($branches as $branch): ?>
		<tr>
			<td><?php echo h($branch['Branch']['id']); ?>&nbsp;</td>
			<td><?php echo h($branch['Branch']['name']); ?>&nbsp;</td>
			<td><?php echo h($branch['Branch']['phone']); ?>&nbsp;</td>
			<td class="actions">
				<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $branch['Branch']['id'])); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div class="pagination pagination-sm">
		<ul class="pagination"><?php
			echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li',
				'class' => 'disabled','disabledTag' => 'a'));
			echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active',
				'tag' => 'li','first' => 1));
			echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, 
				array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			?>
		</ul>
	</div>
	<p>
	<?php
		echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records 
			out of {:count} total, starting on record {:start}, ending on {:end}')));
	?>      
	</p>
</div>
