<!-- Main Navbar across the top. Incorporates banner image. Guest navigation only, plus login info -->
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><?php echo $this->Html->link('Home', array('controller' => 'pages', 'action'=>'home'));?> </li>
				<li><?php echo $this->Html->link('Products', array('controller' => 'pages', 'action' =>'products'));?> </li>
				<li><?php echo $this->Html->link('Loans', array('controller' => 'pages', 'action' => 'loans')); ?> </li>
				<li><?php echo $this->Html->link('Contact Us', array('controller' => 'contacts', 'action' => 'index')); ?> </li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
			<?php
			if ($this->Session->read('Auth.User')){
				$username = AuthComponent::user('username');
				$id = AuthComponent::user('customer_id');
				$role = AuthComponent::user('role');
				if($role == 'ADMIN') {
					echo "<li>";
					echo $this->Html->link('Logged In As '.$username, array ('controller' => 'users', 'action' => 'index', $id ));
					echo "</li>";}
				else {
					echo "<li>";
					echo $this->Html->link('Logged In As '.$username, array ('controller' => 'customers', 'action' => 'edit', $id ));
					echo "</li>";}
					echo "<li>";
					echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));
					echo "</li>";
			}
			else{
					echo "<li>";
					echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login'));
					echo "</li>";}
			?>
			</ul>
		</div>
	</div>
</nav>
<?php echo $this->Html->image('banner.jpg'); ?>

<!-- If Customer or Admin is logged in display the appropriate menu -->
<?php
	if ($this->Session->read('Auth.User')){
		$role = AuthComponent::user('role');
		if($role == 'ADMIN') {echo $this->element('usermenu');}
		else if ($role === 'CUSTOMER') {echo $this->element('customermenu');}
	}
?>

<script src='http://digitalbush.com/wp-content/uploads/2014/10/jquery.maskedinput.js' type='text/javascript'> </script>
<script>
$(document).ready( function() {
$('.dropdown-toggle').dropdown();
$('#mobile').mask('9999-999-999');
$('#phone').mask('(99)9999-9999');
$('#branchphone').mask('(99)9999-9999').bind("blur", function(){
      
      var phone=$(this).val();
      phone=phone.replace("(","");
      phone=phone.replace(")","");
      phone=phone.replace("-","");
      if (phone.length==0)
      {
          alert("please enter a valid phone number");
      }
  });
});
</script>
