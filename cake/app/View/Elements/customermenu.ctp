<!-- The menu that appears when a customer is logged in. Has their customer navigation to get around -->
<nav class="navbar navbar-inverse">
<div class="container-fluid">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
	<li><?php 
		$id = AuthComponent::user('customer_id');
		echo $this->Html->link('Edit my Profile',  array('controller' => 'customers', 'action' => 'edit', $id))?>;</li>
	<li><?php echo $this->Html->link('My Accounts', array('controller' => 'accounts', 'action' => 'index'));?></li>
	<li><?php echo $this->Html->link('My Transactions', array('controller' => 'transactions', 'action' => 'index'));?></li>
      </ul>
  </div>
</div>
</nav>
