<!-- The menu that is displayed when an admin is logged in -->
<nav class="navbar navbar-inverse">
        <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                        <ul class="nav navbar-nav">
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                Users <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li> <?php echo $this->Html->link('Add User', array ('controller' => 'users',
                                                        'action' => 'add')); ?></li>
                                                <li><?php echo $this->Html->link('View Users', array ('controller' => 'users',
                                                        'action' => 'index')); ?></li>
                                        </ul>
                                </li>
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                Customers <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><?php echo $this->Html->link('Add Customer', array ('controller' => 'customers',
                                                        'action' => 'add')); ?></li>
                                                <li><?php echo $this->Html->link('View Customers', array ('controller' => 'customers',
                                                        'action' => 'index')); ?></li>
                                        </ul>
                                </li>
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                                                >Accounts <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><?php echo $this->Html->link('Add Account', array ('controller' => 'accounts',
                                                        'action' => 'add')); ?> </li>
                                                <li><?php echo $this->Html->link('View Accounts', array ('controller' => 'accounts',
                                                        'action' => 'index')); ?> </li>
                                        </ul>
                                </li>
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											Branches <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><?php echo $this->Html->link('Add Branch', array ('controller' => 'branches', 
													'action' => 'add')); ?> </li>
                                                <li><?php echo $this->Html->link('View Branch', array ('controller' => 'branches', 
													'action' => 'index')); ?> </li>
                                        </ul>
                                </li>
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											Interest <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><?php echo $this->Html->link('Add Interest', array ('controller' => 'interests', 
													'action' => 'add')); ?> </li>
                                                <li><?php echo $this->Html->link('View Interest', array ('controller' => 'interests', 
													'action' => 'index')); ?> </li>
                                        </ul>
                                </li>
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Transactions 
											<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><?php echo $this->Html->link('View Transactions', array ('controller' => 'transactions', 
													'action' => 'index')); ?> </li>
                                        </ul>
                                </li>
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Transaction Types 
											<span class="caret"></span>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><?php echo $this->Html->link('Add Transaction Types', array ('controller' => 'transactionTypes', 
													'action' => 'add')); ?></li>
                                                <li><?php echo $this->Html->link('View Transaction Types', array ('controller' => 'transactionTypes', 
													'action' => 'index')); ?> </li>
                                        </ul>
                                </li>
                                <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Account Types 
											<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><?php echo $this->Html->link('Add AccountType', array ('controller' => 'accountTypes', 'action' => 
													'add')); ?> </li>
                                                <li><?php echo $this->Html->link('View Account Types', array ('controller' => 'accountTypes', 'action' => 
													'index')); ?> </li>
                                        </ul>
                                </li>
                        </ul>
                </div>
        </div>
</nav>
