<!-- The 404 of thisbank. Any errors redirect to this page -->
<br/>
<?php
	if ($this->Session->read('Auth.User')){
		echo "<br/>";
		echo "<div class='alert alert-dismissible alert-danger'>";
		echo "<strong>Oh snap!</strong>";
		echo "<br/><br/>";
		echo "Did you really think you could access that?";
		echo "</div>";
	}
	else {
		echo "<div class='alert alert-dismissible alert-danger'>";
		echo "<p>Oops, you aren't allowed to see that.</p>";
		echo "<p>You need to ";
		echo $this->Html->Link ('Login', array ('controller' => 'users', 'action' => 'login'));
		echo " be logged in to view that page</p>";
		echo "</div>";
	}
?>

