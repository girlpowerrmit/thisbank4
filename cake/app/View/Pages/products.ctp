<!-- Products information page. Cosmetic purposes only --> 
<div class = "container">
	<h2> Products </h2>
	<br/>
	<div class = "col-lg-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Bank Accounts</h3>
			</div>
			<div class="panel-body">
				<p>Savings</p>
				<p>Cheque</p>
				<p>Loan</p>
				<p>Term Deposit</p>
			</div>
		</div>
	</div>
	<div class = "col-lg-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Online Banking</h3>
			</div>
			<div class="panel-body">
				<p>View Accounts and Balances</p>
				<p>View Transactions</p>
				<p>Transfer Money</p>
				<p>Update your Information</p>
			</div>
		</div>
	</div>
	<div class = "col-lg-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Loans</h3>
			</div>
			<div class="panel-body">
				<p>Home Loans</p>
				<p>Personal Loans</p>
				<p>Competitive Interest Rates</p>
			</div>
		</div>
	</div>
</div>
