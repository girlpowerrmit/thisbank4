<!-- The Landing Page for the website. -->
<div class = "container">
   <div class = "row">
	<div class = "col-md-4">
                        <h3> View Your Accounts </h3>
                        <h5> View the balance of all your registered online banking accounts <h5>
			<?php echo $this->Html->image('dollar.jpg');?>
	</div>
	<div class = "col-md-4">
			<h3> Transfer Money </h3>
			<h5> Transferring money between your accounts couldn't be easier or quicker <h5>
			<?php echo $this->Html->image('notes.jpg');?>
	</div>
	<div class = "col-md-4">
			<h3> View Your Transactions </h3>
			<h5> View your transactions and search previous transactions by date and description <h5>
			<?php echo $this->Html->image('house.jpg');?>
	</div>
   </div>
</div>
