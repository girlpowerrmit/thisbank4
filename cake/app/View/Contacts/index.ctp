<!-- Contact Us Page, Doesn't mail out the form, just for cosmetic purposes -->
<h2> Contact Us </h2>
	<?php
		echo $this->Form->create('Contact',array(
			'controller' => 'contact', 'action' => 'index',
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));

	echo $this->Form->input('name', array('placeholder' => 'name (required)', 'onfocus' => 'clearDefault(this)'));
	echo $this->Form->input('email', array('placeholder' => 'email (required)', 'onfocus' => 'clearDefault(this)'));
	echo $this->Form->input('message', array('placeholder' => 'message', 'onfocus' => 'clearDefault(this)'));
	echo $this->Form->submit();
	echo $this->Form->end();
	?>
</div>

