<!-- Add Customer Form. Builds a form with the fields and Address fields for the new customer -->
<div class="customers form">
	<h2><?php echo __('Add Customer'); ?></h2>
	<?php
		echo $this->Form->create('Customer',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
	?>
	<fieldset>
	<?php
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('dob', array('class' => 'searchDate', 'label' => 'Date of Birth'
			, 'dateFormat' => 'DMY', 'minYear' => date('Y') - 110, 'maxYear' => date('Y') - 0));
		echo $this->Form->input('email');
		echo $this->Form->input('phone');
		echo $this->Form->input('mobile');
		echo $this->Form->input('active', array('checked' => 1,));
		echo $this->Form->input('CustomerAddress.0.address_line_1');
		echo $this->Form->input('CustomerAddress.0.suburb');
		echo $this->Form->input('CustomerAddress.0.post_code');
		echo $this->Form->input('CustomerAddress.0.state', array('options'=>array('VIC','NSW','QLD','WA','NT','ACT',
			'SA','TAS'),'State'));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>

