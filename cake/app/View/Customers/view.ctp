<!-- Customers View Page, not accessible in navigation but directly via URL by admin -->
<!-- Displays related accounts and users for that customer that you have chosen to view -->
<div class="customers view">
	<h2><?php echo __('View Customer'); ?></h2>
	<div class="related">
	<h3><?php echo __('Related Accounts'); ?></h3>
	<?php if (!empty($customer['Account'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Account Number'); ?></th>
				<th><?php echo __('Branch Id'); ?></th>
				<th><?php echo __('Open Date'); ?></th>
				<th><?php echo __('Account Type'); ?></th>
				<th><?php echo __('Balance'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php
			$i=0;
			foreach ($customer['Account'] as $account): ?>
			<tr>
				<td><?php echo $account['id']; ?></td>
				<td><?php echo $account['Branch']['name']; ?></td>
				<td><?php echo date('d-m-Y',strtotime($account['open_date'])); ?></td>
				<td><?php echo $account['AccountType']['description']; ?></td>
				<td><?php echo "$".number_format($CurrentBalance[$i],2); $i++;?></td>
				<td class="actions">
					
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'accounts', 'action' => 'edit', $account['id'])); ?>
					
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	<div class="related">
		<h3><?php echo __('Related Users'); ?></h3>
		<?php if (!empty($customer['User'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Role'); ?></th>
				<th><?php echo __('Username'); ?></th>
				<th><?php echo __('Created'); ?></th>
				<th><?php echo __('Modified'); ?></th>
				<th><?php echo __('Customer Id'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php
			$i = 0;
			foreach ($customer['User'] as $user): ?>
			<tr>
				<td><?php echo $user['id']; ?></td>
				<td><?php echo $user['role']; ?></td>
				<td><?php echo $user['username']; ?></td>
				<td><?php echo $user['created']; ?></td>
				<td><?php echo $user['modified']; ?></td>
				<td><?php echo $user['customer_id']; ?></td>
				<td class="actions">
					
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
					
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
		<?php endif; ?>
	</div>
</div>
