<!-- Customers Index View Page, Display all customers and a search form. This page is only available to admin -->
<div class="customers index">
	<div class = "row">
		<div class = "col-md-4">
			<div class="filters">
				<h3>Search Customers</h3>
				<?php
				$base_url = array('controller' => 'Customers', 'action' => 'index');
				echo $this->Form->create('search',array(
					'url' => $base_url,
					'class' => 'form-group',
					'inputDefaults' => array(
					'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
					'div' => array('class' => 'control-group'),
					'label' => array('class' => 'control-label'),
					'between' => '<div class="controls">',
					'after' => '</div>',
					'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
				)));

				echo $this->Form->input("first_name", array('label' => 'First Name', 'placeholder' => "eg John"));
				echo $this->Form->input("last_name", array('label' => 'Last Name', 'placeholder' => "eg Smith"));
				echo $this->Form->input("email", array('label' => 'Email', 'placeholder' => "Email@address"));
				echo $this->Form->input("mobile", array('label' => 'Mobile', 'placeholder' => "eg 0404999495"));
				echo $this->Form->input("post_code", array('label' => 'Post Code', 'placeholder' => "eg 2000"));
				echo $this->Form->input('accountNo', array('style' => 'height:30px', 'label' => 'Account Number', 'type'=>'number','size' => '10', 'placeholder' => 'Customer Account ID eg. 19'));
				echo $this->Form->input('datestart', array('class' => 'searchDate', 'label' => 'DoB From', 'type'=>'date', 'minYear' => 1900, 'maxYear' => date('Y'), 'dateFormat' => 'Y-M-D', 'selected' => '1900-01-01'));
				echo $this->Form->input('dateend', array('class' => 'searchDate', 'label' => 'DoB To', 'type'=>'date', 'minYear' => 1900, 'maxYear' => date('Y'), 'dateFormat' => 'Y-M-D',  'selected' =>date('Y-m-d')));
				echo $this->Form->input('active', array('label' => 'Active', 'options'=>array(1=>'Yes',0=>'No', 2=>'Both'), 'selected' => 2));

				// Search!
				echo $this->Form->submit("Search");

				// To reset all the filters we only need to redirect to the base_url
				echo "<div class='submit actions'>";
				echo $this->Html->link("Reset",$base_url);
				echo "</div>";
				echo $this->Form->end();
				?>
				<?php #echo $this->element('sql_dump'); ?>
			</div>
		</div>
		<div class = "col-md-8">
		<br/>
		<?php echo $this->Html->link('Add Customer', array('controller' => 'customers', 'action' => 'add'),
                        array('bootstrap-type' => 'primary', 'class' => 'btn btn-primary', 'rule' => 'button'));?>

		<h2><?php echo __('Customers'); ?></h2>

		<table class="table table-striped table-hover">
			<tr>
				<th><?php echo $this->Paginator->sort('first_name'); ?></th>
				<th><?php echo $this->Paginator->sort('last_name'); ?></th>
				<th><?php echo $this->Paginator->sort('dob', 'Date of Birth'); ?></th>
				<th><?php echo $this->Paginator->sort('email'); ?></th>
				<th><?php echo $this->Paginator->sort('phone'); ?></th>
				<th><?php echo $this->Paginator->sort('mobile'); ?></th>
				<th><?php echo $this->Paginator->sort('active'); ?></th>
				<th class="actions"><?php  ?></th>
			</tr>
			<?php foreach ($customers as $customer): ?>
			<tr>
				<td>
				<?php echo $this->Html->link(__($customer['Customer']['first_name']), array('action' => 'edit', ($customer['Customer']['id']))); ?> &nbsp;
				</td>
				<td>
				<?php echo $this->Html->link(__($customer['Customer']['last_name']), array('action' => 'edit', ($customer['Customer']['id']))); ?> &nbsp;
				</td>
				<td><?php echo date('d-m-Y', strtotime(h($customer['Customer']['dob']))); ?>&nbsp;</td>
				<td><?php echo h($customer['Customer']['email']); ?>&nbsp;</td>
				<td><?php echo h($customer['Customer']['phone']); ?>&nbsp;</td>
				<td><?php echo h($customer['Customer']['mobile']); ?>&nbsp;</td>
				<td><?php 
					if(h($customer['Customer']['active']) == 1) {echo 'Active';}
					else {echo 'Inactive';}; ?>
				</td>
				<td class="actions">
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $customer['Customer']['id'])); ?>
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $customer['Customer']['id'])); ?>
                                </td>
			</tr>
			<?php endforeach; ?>
		</table>
		<div class="pagination pagination-sm">
			<ul class="pagination">
			<?php
				echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
				echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
			?>
			</ul>
		</div>
		<p>
		<?php
			echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
		?>      
		</p>
	</div>
</div>
