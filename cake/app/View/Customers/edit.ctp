<!-- Edit Customer View. Also displays their addresses associated with the Customer --> 
<div class="customers form">
	<h2><?php echo __('Edit Customer'); ?></h2>
	<?php
		echo $this->Form->create('Customer',array(
			'class' => 'form-group',
			'inputDefaults' => array(
			'format' => array('before','label','between','input' => array('class' => 'form-group'),'error','after'),
			'div' => array('class' => 'control-group'),
			'label' => array('class' => 'control-label'),
			'between' => '<div class="controls">',
			'after' => '</div>',
			'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
		)));
	?>
	<fieldset>
		<?php
			$options= array(
			'label' => 'Submit',
			'id'=>'submit',
			'div' => FALSE,
			'onclick'=>'return updatePhone()'
			);

		echo $this->Form->input('Customer.id');
		echo $this->Form->input('Customer.first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('dob', array('class' => 'searchDate', 'disabled'=>'disabled')); // shouldn't change this field after the account is active
		echo $this->Form->input('email');
		echo $this->Form->input('phone', array('id'=>'phone'));
		echo $this->Form->input('mobile', array('id'=>'mobile'));
                
                // if Customer logs in, this field is read only. 
		if($this->Session->check('Customer'))  {
			echo $this->Form->input('active', array('disabled'=>'disabled'));
                        
                }
                
                //if Admin logs in, this field is visable
		else if($this->Session->check('Admin')) {
			echo $this->Form->input('active');}

		$count=0;
			foreach ($this->data['CustomerAddress'] as $customerAddress) {
			$addCount = $count+1;
			echo '<h3> Address ' . $addCount . '</h3>';
			echo $this->Form->hidden('CustomerAddress.'.$count.'.id');
			echo $this->Form->input('CustomerAddress.'.$count.'.address_line_1');
			echo $this->Form->input('CustomerAddress.'.$count. '.suburb');
			echo $this->Form->input('CustomerAddress.'. $count. '.post_code');
			echo $this->Form->input('CustomerAddress.0.state', array('options'=>array('VIC' => 'VIC','NSW'=>'NSW','QLD'=>'QLD','WA'=>'WA','NT'=> 'NT','ACT'=>'ACT','SA' => 'SA', 'TAS' => 'TAS', 'empty' => 'Choose one'),'State'));
			echo $this->Html->link(__('Delete'), array('action' => 'removeAddress',$customerAddress['id'], $this->data['Customer']['id']),
			null, __('Are you sure you want to delete # %s?', $customerAddress['id'])

			);
			$count++;
		}

		$custID = $this->data['Customer']['id'];
		echo '<br/><br/>';
		echo $this->Html->link("Add Address", array('controller' => 'customerAddresses', 'action' => 'add', $custID));
		?>
	</fieldset>
	<?php echo $this->Form->end($options); ?>
</div>

<script type="text/javascript">
	function updatePhone() {
		var phone=document.getElementById('phone').value;
		var mobile=document.getElementById('mobile').value;
		phone=phone.replace("(","");
		phone=phone.replace(")","");
		phone=phone.replace("-","");
		mobile=mobile.replace(/-/g,"");
		document.getElementById('phone').value=phone;
		document.getElementById('mobile').value=mobile;
		return true;
	}
</script>
