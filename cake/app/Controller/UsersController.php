<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The users controller controls the users functionality for the site.
 * This is key functionality as it controls the access of the person
 * to various views based on their role. A user can be a customer or an admin.
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    // Only logged in users can access the User pages.
    
    public function isAuthorized($user) {
        if ($this->Auth->user('role') === 'ADMIN') {
            return true;
        }
        return parent::isAuthorized($user);
    }

    // Login functionality 
    
    public function login() {


        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $role = $this->Auth->user('role');
                if ($role === 'ADMIN') {
                    $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'index')
                    );
                } else {
                    $id = $this->Auth->user('customer_id');
                    $this->redirect(array(
                        'controller' => 'accounts',
                        'action' => 'index',
                        $id)
                    );
                }
            }
            $this->Session->setFlash(__('Invalid username or password, try again'));
        }
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    // Logout functionality
    
    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * index method
     * Allows the administrator to view a list of users.
     * Allows the administrator to search for userse based on name, customer,
     * and date created.
     * @return void
     */
    public function index() {

        $conditions = array();
        $datestart = NULL;
        $dateend = NULL;

        if (($this->request->is('post') || $this->request->is('put')) &&
                isset($this->data['search'])) {

            foreach ($this->data['search'] as $param_name => $value) {
                
                // Search by data created.

                if ($param_name == 'datestart' && $value != NULL) {
                    $datestart = $value['year'] . '-' . $value['month'] . '-' . $value['day'];
                    
                } elseif ($param_name == 'dateend' && $value != NULL) {
                    $dateend = $value['year'] . '-' . $value['month'] . '-' . $value['day'];
                    
                } 
                // Search by customer name
                elseif ($param_name == 'name') {
                    if ($value != NULL) {
                        $this->loadModel('Customers');
                        $name = $this->data['search']['name'];

                        $conditions[] = array('(CONCAT(`Customer`.`first_name`, " ", '
                            . '`Customer`.`last_name`)) LIKE'
                            => '%' . $name . '%');
                    }
                } else 
                    // Search by user name
                    {
                    $conditions[] = array('User.' .
                        $param_name . ' LIKE' => $value . '%');
                }
            }

            $conditions[] = array('date(User.created) >= ' => $datestart, 
                'date(User.created) <= ' => $dateend);
        }

        $this->User->recursive = 0;

        $this->paginate = array('conditions' => $conditions);
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * view method
     * Allows the admin to view the details of the user.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     * Allows the admin to create new users. Usernames must be unique.
     * Administrators do not need a customer ID, but customer roles do.
     * @return void
     */
    public function add() {

        if ($this->request->is('post')) {

            $searchRole = $this->request->data['User']['role'] == "ADMIN";

            if ($searchRole == "ADMIN") {
                // If teh role is an administrator, customer not required.
                $this->User->validate = $this->User->validateForAdmin;
            }

            $this->User->create();

            if ($this->User->save($this->request->data)) {


                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
        $customers = $this->User->Customer->find('list', array('fields' => array('Customer.id', 'Customer.name')));
        $this->set(compact('customers'));
    }

    /**
     * edit method
     * Allows the admin to edit a user. 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->User->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $searchRole = $this->request->data['User']['role'] == "ADMIN";

            if ($searchRole == "ADMIN") {

                $this->User->validate = $this->User->validateForAdmin;
            }

            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $customers = $this->User->Customer->find('list');
        $this->set(compact('customers'));
    }

}
