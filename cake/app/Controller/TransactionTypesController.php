<?php

App::uses('AppController', 'Controller');

/**
 * TransactionTypes Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: Each transaction has a transaction type (credit or debit).
 * @property TransactionType $TransactionType
 * @property PaginatorComponent $Paginator
 */
class TransactionTypesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    // Only logged in users (administrators) can view transaction types
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    public function isAuthorized($user) {
        return parent::isAuthorized($user);
    }

    /**
     * index method
     * Shows a list of transaction types.
     * @return void
     */
    public function index() {
        $this->TransactionType->recursive = 0;
        $this->set('transactionTypes', $this->Paginator->paginate());
    }

    /**
     * view method
     * Allows administrators to view the transaction type. 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->TransactionType->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        $options = array('conditions' => array('TransactionType.' . 
            $this->TransactionType->primaryKey => $id));
        $this->set('transactionType', $this->TransactionType->find('first', $options));
    }

    /**
     * add method
     * Allows administrators to add a transaction type. 
     * Transaction type descriptions must be unique.
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->TransactionType->create();
            if ($this->TransactionType->save($this->request->data)) {
                $this->Session->setFlash(__('The transaction type has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The transaction type could not be saved.'
                        . ' Please, try again.'));
            }
        }
    }

    /**
     * edit method
     * Allows the administrator to edit existing transaction types. 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->TransactionType->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->TransactionType->save($this->request->data)) {
                $this->Session->setFlash(__('The transaction type has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The transaction type could not be '
                        . 'saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('TransactionType.' .
                $this->TransactionType->primaryKey => $id));
            $this->request->data = $this->TransactionType->find('first', $options);
        }
    }

}
