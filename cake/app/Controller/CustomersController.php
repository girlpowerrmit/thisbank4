<?php

App::uses('AppController', 'Controller');

/**
 * Customers Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: A customer is an individual who has a bank account with $this->$Bank.
 * This controller contains the logic that relates to the Customer pages.
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class CustomersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    
    // Customers can only view customer information about themselves. 
    // The customer_id is a property of the Users model.
    
    public function isAuthorized($user) {
        if ($this->Auth->user('role') === 'CUSTOMER') {
            if (in_array($this->action, array('edit'))) {
                $id = (int) $this->params['pass'][0];
                $user = (int) $this->Auth->User('customer_id');
                if ($id === $user) {
                    return true;
                }
            }
            
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        return parent::isAuthorized($user);
    }

    public $components = array('Paginator', 'Session');
    
    // Only logged in users can view these pages. 

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    /**
     * index method
     * Displays a list of customers to the Administrator.
     * Allows the administrator to search on fields such as first name,
     * last name, date of birth, phone. 
     * @return void
     */
    public function index() {

        $conditions = array();
        
        // Check to see there is data in the search fields and the request is POST.
        
        if (($this->request->is('post') || $this->request->is('put')) && 
                isset($this->data['search'])) {
            $datestart = NULL;
            $dateend = NULL;

            foreach ($this->data['search'] as $param_name => $value) {
                
                // Search on dob range.
                if ($param_name == 'datestart' && $value != NULL) {
                    $datestart = $value['year'] . '-' . $value['month'] . '-' 
                            . $value['day'];
                } elseif ($param_name == 'dateend' && $value != NULL) {
                    $dateend = $value['year'] . '-' . $value['month'] . '-' 
                            . $value['day'];
                    
                } elseif ($param_name == 'active') {
                    // 2 is the ID of both, if both is selected, show inactive and active.
                    if ($value <> 2) {
                        $conditions[] = array('Customer.active' => $value);
                    }
                    
                    // Search by customers accounts. 
                } elseif ($param_name == 'accountNo') {
                    if ($value != NULL) {
                        $this->loadModel('Accounts');
                        $accountNo = $this->data['search']['accountNo'];
                        $matchAccount = $this->Accounts->find('first', array(
                            'conditions' => array('Accounts.id' => $accountNo)
                        ));
                        $conditions[] = array('Customer.id' => 
                            $matchAccount['Accounts']['customer_id']);
                    }
                    
                    // Post code search.
                } elseif ($param_name == 'post_code') {
                    if ($value != NULL) {
                        $this->loadModel('CustomerAddresses');
                        $postCode = $this->data['search']['post_code'];
                        $conditionsSubQuery['`Customer_Addresses`.`post_code`'] = $postCode;

                        // This query looks at the customer address table to 
                        // get customer ID's with a post code inputted in the search.
                        $db = $this->CustomerAddresses->getDataSource();
                        $subQuery = $db->buildStatement(
                                array(
                            'fields' => array('`Customer_Addresses`.`customer_id`'),
                            'table' => $db->fullTableName($this->CustomerAddresses),
                            'alias' => 'Customer_Addresses',
                            'limit' => null,
                            'offset' => null,
                            'joins' => array(),
                            'conditions' => $conditionsSubQuery,
                            'order' => null,
                            'group' => null
                                ), $this->CustomerAddress
                        );
                        $subQuery = ' `Customer`.`id` IN (' . $subQuery . ') ';
                        $subQueryExpression = $db->expression($subQuery);

                        $conditions[] = $subQueryExpression;
                    }
                } else {
                    $conditions[] = array('Customer.' . $param_name . 
                        ' LIKE' => $value . '%');
                }
            }
            $conditions[] = array('Customer.dob >= ' => $datestart, 
                'Customer.dob <= ' => $dateend);
        }
        $this->Customer->recursive = 0;
        $this->paginate = array('conditions' => $conditions);
        $this->set('customers', $this->Paginator->paginate());
    }

    /**
     * view method
     * Allows the administrator to view the detail of the customer, including the balance 
     * of each related account.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Customer->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        $options = array('conditions' => array('Customer.' .
            $this->Customer->primaryKey => $id), 'recursive' => 2);


        $data = $this->Customer->find('first', $options);
        $this->set('customer', $data);
        
        // Calculate the current balance for the related accounts list. 
        $CurrentBalance = array();
        
        foreach ($data['Account'] as $account) {
            $balance = 0;
            foreach ($account['Transaction'] as $transaction) {
                if ($transaction['transaction_type_id'] == 1) {
                    $balance+=$transaction['amount'];
                } else {
                    $balance-=$transaction['amount'];
                }
            }


            $CurrentBalance[] = $balance;
        }

        $this->set('CurrentBalance', $CurrentBalance);
    }

    /**
     * add method
     * Allows an administrator to create a new customer. 
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {

            $this->Customer->create();


            if ($this->Customer->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('The customer has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The customer could not be saved. '
                        . 'Please, try again.'));
            }
        }
    }

    /**
     * edit method
     * Allows the customer or administrator to edit the customer via the customer profile. 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {


        if (!$this->Customer->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->Customer->saveAll($this->request->data)) {
                $this->Session->setFlash(__('The customer profile has been saved'));
                return $this->redirect(array('controller' => 'accounts', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The customer could not be saved. '
                        . 'Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Customer.' . 
                $this->Customer->primaryKey => $id));
            $this->request->data = $this->Customer->find('first', $options);
        }
    }

    /*
    * removeAddress method
    * This allows an administrator or customer to remove address from the
    * customer profile screen, takes the parameters of customer ID and address ID
    * @param string $id
    * $param string $customerID
    * @return void
    */
    public function removeAddress($id = null, $customerID = null) {

        if ($this->Auth->user('role') === 'CUSTOMER') {
            $user = (int) $this->Auth->User('customer_id');
            if ($user <> $customerID) {
                $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
            }
        }
        
        // Get a list of customer addresses for this customer. 
        $this->loadModel('CustomerAddresses');
        $customerAddresses = $this->Customer->CustomerAddress->find('first', 
                array('conditions' => array('CustomerAddress.id' => $id),
            'CustomerAddress.customer_id' => $customerID
        ));

        // Finds all customer addresses and those that match the ID
        // selected for deletion will be deleted. 
        foreach ($customerAddresses as $customerAddress) {

            if ($this->Customer->CustomerAddress->delete($id)) {
                $this->Session->setFlash(__('The customer address has been deleted'));
                return $this->redirect(array('action' => 'edit', $customerID));
            }
            $this->Session->setFlash(__('Customer address was not deleted'));
            return $this->redirect(array('action' => 'edit', $customerID));
        }
    }

}
