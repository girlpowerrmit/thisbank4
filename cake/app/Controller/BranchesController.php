<?php

App::uses('AppController', 'Controller');

/**
 * Branches Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The Branches controller controls the logic for the Branches pages.
 * This allows an administrator to add and edit existing bank branches (physical
 * locations of the bank). 
 * @property Branch $Branch
 * @property PaginatorComponent $Paginator
 */
class BranchesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    
    // Branch is for logged in users only

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    public function isAuthorized($user) {
        return parent::isAuthorized($user);
    }

    /**
     * index method
     * Displays a list of branches. Restricted to ADMIN only.
     * @return void
     */
    public function index() {
        $this->Branch->recursive = 0;
        $this->set('branches', $this->Paginator->paginate());
    }

    /**
     * view method
     * Displays the branch name and phone. Restricted to ADMIN only. 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Branch->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        $options = array('conditions' => array('Branch.' . $this->Branch->primaryKey => $id));
        $this->set('branch', $this->Branch->find('first', $options));
    }

    /**
     * add method
     * Allows the administrator to add new branches. Name and phone are mandatory.
     * Branch names must be unique. 
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Branch->create();
            if ($this->Branch->save($this->request->data)) {
                $this->Session->setFlash(__('The branch has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The branch could not be saved. Please, try again.'));
            }
        }
       
    }

    /**
     * edit method
     * Allows the administrator to edit existing branches. Note - branches
     * cannot be deleted due to referential integrity issues.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Branch->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Branch->save($this->request->data)) {
                $this->Session->setFlash(__('The branch has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The branch could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Branch.' . $this->Branch->primaryKey => $id));
            $this->request->data = $this->Branch->find('first', $options);
        }
    }

}
