<?php

App::uses('AppController', 'Controller');

/**
 * Interests Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The purpose of the interest controller is to control the pages
 * that display and edit the interest. An interest rate is associated to
 * an account type and can be used to periodically apply interest fees
 * to an account. An interest is considered active when the end date is null.
 * When an interest is de-activated the end date is set to today's date. When a
 * new interest is added for a particular account type, 
 * the end date is also set to today's date regardless of its status. 
 * @property Interest $Interest
 * @property PaginatorComponent $Paginator
 */
class InterestsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     * Only logged in users (administrators) can see these pages.
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    public function isAuthorized($user) {
        return parent::isAuthorized($user);
    }

    /**
     * index method
     * Shows a list of all interests. 
     * @return void
     */
    public function index() {
        $this->Interest->recursive = 0;
        $this->set('interests', $this->Paginator->paginate());
    }

    /**
     * view method
     * Displays the details of an interest.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Interest->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }

        $options = array('conditions' => array('Interest.' . $this->Interest->primaryKey => $id));
        $this->set('interest', $this->Interest->find('first', $options));
    }

    /**
     * add method
     * Allows the administrator to add a new interest rate.
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Interest->create();
            if ($this->Interest->save($this->request->data)) {
                $oldInterest = $this->Interest->find('first', array('conditions' => array(
                        'account_type_id' => 
                    $this->request->data['Interest']['account_type_id'],
                        'end_date' => null
                )));
                //If there is an old interest with the linked account, that 
                //interest will have today's end date attached.
                if ($oldInterest != null) {
                    $oldInterest['Interest']['end_date'] = date('y-m-d');

                    $this->Interest->save($oldInterest);
                }

                $this->Session->setFlash(__('The interest has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The interest could not be saved. '
                        . 'Please, try again.'));
            }
        }
        $accountTypes = $this->Interest->AccountType->find('list');
        $this->set(compact('accountTypes'));
    }

    /**
     * edit method
     * Allows the administrator to edit an existing interest.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Interest->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }

        // Allows the administrator to click a Deactivate checkbox to set the 
        // end date to today's date.
        
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->request->data['deactivate'] == true) {
                $now = date('Y-m-d H:i:s');
                $this->Interest->set('end_date', $now);
            } else {
                $this->Interest->set('end_date', null);
            }
            if ($this->Interest->save($this->request->data)) {
                $this->Session->setFlash(__('The interest has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The interest could not be saved. '
                        . 'Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Interest.' . $this->Interest->primaryKey => $id));
            $this->request->data = $this->Interest->find('first', $options);
        }
        $accountTypes = $this->Interest->AccountType->find('list');
        // Show the account types in the drop down.
        $this->set(compact('accountTypes'));
    }

}
