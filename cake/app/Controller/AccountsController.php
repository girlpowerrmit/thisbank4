<?php

App::uses('AppController', 'Controller');

/**
 * Accounts Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The accounts control sets all the logic for the accounts pages,
 * including those that the Customer can see and the back end Administrative
 * access. 
 * @property Account $Account
 * @property PaginatorComponent $Paginator
 */
class AccountsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /*
     * Users must be logged in to view Account pages. 
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    /*
     * Customers can only view Accounts that they are associated with (this is
     * done via the Customer ID in the Users model. Admin have access to all. 
     */
    public function isAuthorized($user) {
        if ($this->Auth->user('role') === 'CUSTOMER') {
            if (in_array($this->action, array('index'))) {
                return true;
            } else if (in_array($this->action, array('view'))) {
                $passid = (int) $this->params['pass'][0];
                $this->Account->id = $passid;
                $customerid = $this->Account->field('customer_id');
                $usercustomer = $this->Auth->User('customer_id');
                if ($customerid === $usercustomer) {
                    return true;
                }
            } else {
                $this->redirect(array('controller' => 'pages', 'action' => 'oops', $user));
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * index method
     * Displays the list of accounts, including the balance. 
     * @return void
     */
    public function index() {

        //Calculate balances
        $accounts = $this->Account->find('all');
        $CurrentBalance = array();

        foreach ($accounts as $account) {
            $balance = 0;
            $id = $account['Account']['id'];
            foreach ($account['Transaction'] as $transaction) {
                if ($transaction['transaction_type_id'] == 1) {
                    $balance+=$transaction['amount'];
                } else {
                    $balance-=$transaction['amount'];
                }
            }


            $CurrentBalance[$id] = $balance;
        }

        $this->set('CurrentBalance', $CurrentBalance);

        // Declare paginator conditions variables
        $conditions = array();
        $datestart = NULL;
        $dateend = NULL;

        // Amend the conditions variable based on the users search input
        if (($this->request->is('post') || $this->request->is('put')) && 
                isset($this->data['search'])) {
            foreach ($this->data['search'] as $param_name => $value) {
                if ($param_name == 'datestart' && $value != NULL) {
                    $datestart = $value['year'] . '-' . $value['month'] . '-' . 
                            $value['day'];
                } elseif ($param_name == 'dateend' && $value != NULL) {
                    $dateend = $value['year'] . '-' . $value['month'] . '-' . 
                            $value['day'];
                } elseif ($param_name == 'branch') {
                    if ($value) {
                        $this->loadModel('Branches');
                        $branch = $this->data['search']['branch'];
                        $conditions[] = array('`Branch`.`name` LIKE'
                            => '%' . $branch . '%');
                    }
                } elseif ($param_name == 'name') {
                    if ($value) {
                        $this->loadModel('Customers');
                        $name = $this->data['search']['name'];
                        $conditions[] = array('(CONCAT(`Customer`.`first_name`, " ", '
                            . '`Customer`.`last_name`)) LIKE'
                            => '%' . $name . '%');
                    }
                } elseif ($param_name == 'account_type_desc') {
                    if ($value) {
                        $this->loadModel('AccountTypes');
                        $type = $this->data['search']['account_type_desc'];
                        $conditions[] = array('`AccountType`.`id`'
                            => $type);
                    }
                }

                // load the Interest model and find the account type ID 
                // which matches the user searched interest rate
                elseif ($param_name == 'interest') {
                    if ($value) {
                        $this->loadModel('Interest');
                        $interest = $this->data['search']['interest'];
                        $conditionsSubQuery['`Interest`.`interest`'] = $interest;

                        $db = $this->Interest->getDataSource();
                        $subQuery = $db->buildStatement(
                                array(
                            'fields' => array('`Interest`.`account_type_id`'),
                            'table' => $db->fullTableName($this->Interest),
                            'alias' => 'Interest',
                            'offset' => null,
                            'joins' => array(),
                            'conditions' => array($conditionsSubQuery,
                                '`Interest`.`end_date` IS NULL'),
                            'order' => null,
                            'group' => null
                                ), $this->Interest
                        );
                        $subQuery = ' `Account`.`account_type_id` IN (' . $subQuery . ') ';
                        $subQueryExpression = $db->expression($subQuery);

                        $conditions[] = $subQueryExpression;
                    }
                } else {
                    $conditions[] = array('Account.' .
                        $param_name . ' LIKE' => $value . '%');
                }
            }

            $conditions[] = array('Account.open_date >= ' => $datestart,
                'Account.open_date <= ' => $dateend);
        }


        $role = $this->Auth->user('role');
        $id = $this->Auth->user('customer_id');
        if ($role === 'ADMIN') {
            $this->Account->recursive = 1;

            $this->set('accounts', $this->paginate($conditions));
        } else if ($role === 'CUSTOMER') {
            $this->paginate = array('conditions' => array('Account.customer_id' => $id,
                    'Account.active' => 1
            ));
            $this->set('accounts', $this->paginate());
        }
    }

    /**
     * View method
     * Allows the user to view the details of the Account including the balance.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Account->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }

        // This is used for when a customer is logged in
        $options = array('conditions' => array('Account.' . 
            $this->Account->primaryKey => $id), 'recursive' => 2);
        $balance = $this->Account->Transaction->Balance($id);
        $customerId = $this->Auth->user('customer_id');
        $this->set('customerId', $customerId);
        $this->set('account', $this->Account->find('first', $options));
        $this->set('balance', $balance);
    }

    /**
     * add method
     * Restricted to ADMIN only.
     * Admin can create a new account. 
     * @return void
     */
    
    public function add($id = null) {
        if ($this->request->is('post')) {

            $this->Account->create();
            // The user does not enter the open date, this is set to today's date.
            $now = date('Y-m-d H:i:s');
            $this->Account->set('open_date', $now);
            if ($this->Account->save($this->request->data)) {
                $this->Session->setFlash(__('The account has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The account could not be saved. Please, try again.'));
            }
        }

        // Get all the values for the drop down lists. 
        
        $branches = $this->Account->Branch->find('list');
        $accountTypes = $this->Account->AccountType->find('list', 
                array('fields' => (array('AccountType.id',
        'AccountType.description'))));
        $customers = $this->Account->Customer->find('list');

        $this->set(compact('branches', 'accountTypes', 'customers'));
    }

    /**
     * edit method
     * Restricted to admin only.
     * Allows the administrator to edit an existing account. Similar to add method.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        
        if (!$this->Account->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Account->save($this->request->data)) {
                $this->Session->setFlash(__('The account has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The account could not be saved. '
                        . 'Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Account.' . $this->Account->primaryKey => $id));
            $this->request->data = $this->Account->find('first', $options);
        }
        $branches = $this->Account->Branch->find('list');
        $accountTypes = $this->Account->AccountType->find('list');
        $customers = $this->Account->Customer->find('list');

        $this->set(compact('branches', 'accountTypes', 'customers'));
    }

}
