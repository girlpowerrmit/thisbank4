<?php

App::uses('AppController', 'Controller');

/**
 * AccountTypes Controller
 *
 * @property AccountType $AccountType
 * @property PaginatorComponent $Paginator
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: Account types relates to the account type model, which allows
 * us to define types of accounts that a customer can own. These account types
 * are savings, cheque etc. Account types also have associated interest rates.
 */
class AccountTypesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /*
     * Account Type access is restricted to logged in users only. 
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    public function isAuthorized($user) {
        return parent::isAuthorized($user);
    }
    
    /**
     * index method
     * Restricted to ADMIN only. 
     * Lists account types on the screen in tabular format
     * @return void
     */

    public function index() {
        $this->AccountType->recursive = 0;
        $this->set('accountTypes', $this->Paginator->paginate());
    }

    /**
     * view method
     * Allows an admin to view the detail of the account type.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->AccountType->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        $options = array('conditions' => array('AccountType.' . 
            $this->AccountType->primaryKey => $id));
        $this->set('accountType', $this->AccountType->find('first', $options));
    }

    /**
     * add method
     * Restricted to ADMIN only. 
     * Allows an admin to add a new account type. Note that duplicate account types
     * have been disabled. You cannot delete an account type as it will have
     * associated accounts.
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->AccountType->create();
            if ($this->AccountType->save($this->request->data)) {
                $this->Session->setFlash(__('The account type has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The account type could not be '
                        . 'saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     * Restricted to ADMIN only. 
     * Allows an admin to edit the account type description. 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->AccountType->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->AccountType->save($this->request->data)) {
                $this->Session->setFlash(__('The account type has been saved'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The account type could not be saved. '
                        . 'Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('AccountType.' .
                $this->AccountType->primaryKey => $id));
            $this->request->data = $this->AccountType->find('first', $options);
        }
    }

}
