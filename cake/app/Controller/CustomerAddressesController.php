<?php

App::uses('AppController', 'Controller');

/**
 * CustomerAddresses Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: A customer may have one or more addresses. The customer 
 * addresses are added and removed via the customer profile. 
 * @property CustomerAddress $CustomerAddress
 * @property PaginatorComponent $Paginator
 */
class CustomerAddressesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    
    // Only logged in users can access the pages.
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    // Customers can only see their own customer addresses.
    
    public function isAuthorized($user) {
        if ($this->Auth->user('role') === 'CUSTOMER') {
            if (in_array($this->action, array('add'))) {
                $id = (int) $this->params['pass'][0];
                $user = (int) $this->Auth->User('customer_id');
                if ($id === $user) {
                    return true;
                }
            }
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        return parent::isAuthorized($user);
    }

    /**
     * index method
     * Displays a list of addresses to the administrator.
     * @return void
     */
    public function index() {
        $this->CustomerAddress->recursive = 0;
        $this->set('customerAddresses', $this->Paginator->paginate());
    }

    /**
     * view method
     * Allows the administrator to view the details of a customer address.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->CustomerAddress->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        $options = array('conditions' => array('CustomerAddress.' .
            $this->CustomerAddress->primaryKey => $id));
        $this->set('customerAddress', $this->CustomerAddress->find('first', $options));
    }

    /**
     * add method
     * Accepts customer ID as a parameter, this is passed through via 
     * the customer profile to allow the user to add customer addresses
     * to the customer profile. 
     * @return void
     */
    public function add($customerID = null) {
        
        // When opening the Add screen, the customer ID will default to 
        // the ID passed from the Customer profile.
        $this->set('selectedCustID', $customerID);

        if ($this->request->is('post')) {
            $this->CustomerAddress->create();

            $this->CustomerAddress->set('customer_id', $customerID);
            if ($this->CustomerAddress->save($this->request->data)) {
                $this->Session->setFlash(__('The customer address has been saved'));
                if ($customerID) {
                    return $this->redirect(array('controller' => 'customers',
                                'action' => 'edit', $customerID));
                }

                return $this->redirect(array('controller' => 'customers',
                            'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The customer address could not be saved. Please, try again.'));
            }
        }
        $customers = $this->CustomerAddress->Customer->find('list');
        $this->set(compact('customers'));
    }

    

}
