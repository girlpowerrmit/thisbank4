<?php

App::uses('AppController', 'Controller');

/**
 * Transactions Controller
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The transactions controller controls access to the transactions
 * pages. A transaction is the movement of funds into, out of, or between
 * a customer's accounts. 
 * @property Transaction $Transaction
 * @property PaginatorComponent $Paginator
 */
class TransactionsController extends AppController {

    /**
     * Components
     * Includes session component (maintains state) and paginator for sorting
     * @var array 
     */
    public $components = array('Paginator', 'Session');

    /**
     * isAuthorised method
     * Checks the role fo the user (Customer or Admin) and grants access
     * to actions. Customers should only be able to see the accounts/transactions
     * etc that they have access to. 
     * @return bool
     */
    public function isAuthorized($user) {
        // Checks for accounts/transactions applicable to Customer ID
        if ($this->Auth->user('role') === 'CUSTOMER') {
            if (in_array($this->action, array('index'))) {
                return true;
            } else if (in_array($this->action, array('add', 'view'))) {
                $passid = (int) $this->params['pass'][0];
                $this->LoadModel('Account');
                $this->Account->id = $passid;
                if (!$this->Account->exists($passid)) {
                    $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
                }
                $customerID = $this->Account->field('customer_id');
                $usercustomer = $this->Auth->user('customer_id');
                if ($customerID === $usercustomer) {
                    return true;
                } else {
                    $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
                }
            } else {
                $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * beforeFilter method
     * Only logged in users can view transactions. 
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('add', 'edit', 'index', 'view');
    }

    /**
     * index method
     * Displays the list of transactions, if a customer is logged in,
     * will only display the transactions for their accounts.
     * @return void
     */
    public function index() {


        $this->LoadModel('Account');

        $conditions[] = array();
        // Amend the conditions variable based on the users search input
        if (($this->request->is('post') || $this->request->is('put')) &&
                isset($this->data['search'])) {
            foreach ($this->data['search'] as $param_name => $value) {
                if ($param_name == 'options' && $value != NULL) {
                    $id = $branch = $this->data['search']['options'];

                    $conditions[] = array('`Account`.`id`'
                        => $id);
                }
            }

            $conditions[] = array('Account.active' => 1);
        }


        $this->Paginator->settings = array(
            'order' => array('Transaction.id' => 'DESC'),
        );
        $role = $this->Auth->user('role');
        $id = $this->Auth->user('customer_id');

        // For admin show all accounts in the drop down and show all transactions
        if ($role === 'ADMIN') {
            $this->set('options', $this->Account->find('list', array(
                        'order' => array('Account.id' => 'asc'),
                        'fields' => 'Account.id, Account.account_description',
                        'recursive' => 0)));
            $this->Transaction->recursive = 0;
            $this->set('transactions', $this->paginate($conditions));
        }
        // For a customer only show accounts and transactions relevant to the customer.
        else if ($role === 'CUSTOMER') {

            $this->set('options', $this->Account->find('list', array('conditions' =>
                        array('Account.customer_id' => $id),
                        'order' => array('Account.id' => 'asc'),
                        'fields' => 'Account.id, Account.account_description', 'recursive' => 0)));


            $this->paginate = array('conditions' => array('Account.customer_id' => $id));
            $this->set('transactions', $this->paginate($conditions));
        }
    }

    /**
     * detail method
     * Allows a user to see the detail of the transaction.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */public function detail($id = null) {
        if (!$this->Transaction->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }
        $options = array('conditions' => array('Transaction.' .
                $this->Transaction->primaryKey => $id));
        $this->set('transaction', $this->Transaction->find('first', $options));
    }

    /**
     * view method
     * Takes the account ID as a parameter and searches transactions based on 
     * user input.
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Transaction->Account->exists($id)) {
            $this->redirect(array('controller' => 'pages', 'action' => 'oops'));
        }

        $conditions = array();
        $conditions[] = array('Account.id' => $id);

        // Find all transactions for the specified account
        $transactions = $this->Transaction->find('all', array('conditions' =>
            array('Account.id' => $id),
            'order' => array('Transaction.id' => 'asc')));


        // Calculate the balance
        $balance = 0;
        $CurrentBalance = array();
        foreach ($transactions as $transaction) {

            if ($transaction['Transaction']['transaction_type_id'] == 1) {
                $balance+=$transaction['Transaction']['amount'];
            } else {
                $balance-=$transaction['Transaction']['amount'];
            }




            $CurrentBalance[$transaction['Transaction']['id']] = $balance;
        }

        $amount = 0;
        $from = 0;
        $to = 0;
        $transactionType = 0;

        // Looks at all the user search parameters and builds conditions
        if ($this->request->is('post')) {
            $description = $this->request->data['Transaction']['description'];

            $startDate = $this->request->data['Transaction']['start_date']['year'] . '-' .
                    $this->request->data['Transaction']['start_date']['month'] . '-' .
                    $this->request->data['Transaction']['start_date']['day'];


            $endDate = $this->request->data['Transaction']['end_date']['year'] . '-' .
                    $this->request->data['Transaction']['end_date']['month'] . '-' .
                    ($this->request->data['Transaction']['end_date']['day'] + 1);

            $from = (double) ($this->request->data['Transaction']['from']);
            $to = (double) ($this->request->data['Transaction']['to']);
            $transactionType = $this->request->data['Transaction']['transaction_type_id'];
            $conditions[] = array('Transaction.description LIKE' => "%$description%");
            $conditions[] = array('(Transaction.transaction_date) >' => $startDate);
            $conditions[] = array('(Transaction.transaction_date) <' => $endDate);

            if ($from > 0) {
                $conditions[] = array('(Transaction.amount) >=' => $from);
            }
            if ($to > 0) {
                $conditions[] = array('Transaction.amount <=' => $to);
            }
            if ($transactionType != "") {
                $conditions[] = array('TransactionType.id' => $transactionType);
            }
            $balance = $this->Transaction->Balance($id, $startDate);
        }

        // Sorts the paginator by transactions descending.
        $this->paginate = array(
            'limit' => 8,
            'conditions' => $conditions,
            'order' => array('Transaction.id' => 'desc')
        );

        // Populates transactions, balance and other form inputs
        $this->set('transactions', $this->paginate());
        $this->set('CurrentBalance', $CurrentBalance);
        $this->set('from', $from);
        $this->set('to', $to);
        $this->set('transactionType', $transactionType);
        $this->set('accountId', $id);

        // Allows the user to clear the form
        if (isset($this->request->data['clear'])) {
            $this->request->data = null;
            $this->redirect(array('action' => 'view', $id));
        }
    }

    /**
     * add method
     * param $id
     * This function exists for allowing transfers between different bank accounts
     * for a customer.
     * Accepts the Account ID as a parameter as all Transactions are associated
     * with one and only one Account.
     * @return void
     */
    public function add($id) {
        if ($this->request->is('post')) {
            $this->Transaction->create();
            //debug($this->request->data);
            $this->request->data['Transaction']['transaction_date'] = date('y-m-d');

            // Gets all the post data and saves it against the Transaction model
            if ($this->Transaction->save($this->request->data)) {
                if (!empty($this->request->data['Transaction']['destination_account'])) {
                    $transaction = array();
                    $transaction['transaction_type_id'] = 1;
                    $transaction['account_id'] = $this->request->data['Transaction']['destination_account'];
                    $transaction['amount'] = $this->request->data['Transaction']['amount'];
                    $transaction['destination_account'] = $id;
                    $transaction['description'] = $this->request->data['Transaction']['description'];
                    $transaction['transaction_date'] = $this->request->data['Transaction']['transaction_date'];

                    $this->Transaction->create();
                    if ($this->Transaction->save($transaction)) {
                        $this->Session->setFlash(__('The transaction has been saved'));
                    } else {
                        $this->Session->setFlash(__('The transaction could not '
                                        . 'be saved. Please, try again.'));
                    }
                }
                return $this->redirect(array('action' => 'view',
                            $this->request->data['Transaction']['account_id']));
                return $this->redirect(array('action' => 'view',
                            $this->request->data['Transaction']['account_id']));
            } else {
                $this->Session->setFlash(__('The transaction could not be saved. Please, try again.'));
            }
        }

        // 
        $customerId = $this->Transaction->Account->find('first', array('conditions' =>
            array('Account.id' => $id), 'recursive' => -1, 'fields' => array('customer_id')));
        $branches = $this->Transaction->Account->find('list', array('conditions' =>
            array('Account.customer_id' => $customerId['Account']['customer_id'],
                'Account.active' => 1,
                'Account.id <>' => $id), 'recursive' => -1, 'fields' => array('id', 'account_description')));

        // Populate the list of accounts that can be transferred to
        $account = $this->Transaction->Account->find('first', array('conditions' =>
            array('Account.id' => $id), 'recursive' => -1));

        // Calculate the balance to display to the customer
        $balance = $this->Transaction->Balance($id);

        $this->set('account', $account);
        $this->set('balance', $balance);
        $this->set('branches', $branches);
    }

}
