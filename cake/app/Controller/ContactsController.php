<?php
/* Authors: Lara Wilson, Melissa Thompson, Christine Zhu
* Purpose: Controller for Contact Us page. Note the e-mail functionality
* does not work due to no e-mail server. */


App::uses('CakeEmail', 'Network/Email');

class ContactsController extends AppController {

    public $components = array('Email', 'Session');

    public function index() {
        if (isset($this->data['Contact'])) {
            $userEmail = $this->data['Contact']['email'];
            $userMessage = $this->data['Contact']['message'];
            
            // No email is actually sent, but show the message to the user
            // their enquiry was sent. 

            $this->Session->setFlash('Thank you for your enquiry: ' . $userEmail . ': ' . $userMessage);
        }
    }

}

?>
