<?php

/**
 * Contact Model
 *
 * @property name
 * @property email
 * @property details
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The Contact Us form allows the customer to contact the branch 
 * (note the email server does not currently work).
 */


class Contact extends AppModel {

    var $name = 'Contacts';
    public $useTable = false;  // Not using the database, of course.
    var $validate = array(
        'name' => array(
            'rule' => '/.+/',
            'allowEmpty' => false,
            'required' => true,
        ),
        'email' => array(
            'allowEmpty' => false,
            'required' => true,
        )
    );
    var $_schema = array(
        'name' => array('type' => 'string', 'length' => 100),
        'email' => array('type' => 'string', 'length' => 255),
        'details' => array('type' => 'text')
    );

}

?>