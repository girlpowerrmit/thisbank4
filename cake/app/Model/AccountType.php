<?php

App::uses('AppModel', 'Model');

/**
 * AccountType Model
 *
 * @property Account $Account
 * @property Interest $Interest
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The account type refers to the type of account (savings, cheque, etc)
 */
class AccountType extends AppModel {
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     * Note: Account descriptions must be unique.
     * @var array
     */
    public $validate = array(
        'description' => array(
            'rule' => 'isUnique',
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Account type description can not be empty and must be unique'
        )
    );
    public $displayField = "description";
    public $hasMany = array(
        'Account' => array(
            'className' => 'Account',
            'foreignKey' => 'account_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Interest' => array(
            'className' => 'Interest',
            'foreignKey' => 'account_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
