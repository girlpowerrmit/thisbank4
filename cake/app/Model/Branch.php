<?php

App::uses('AppModel', 'Model');

/**
 * Branch Model
 *
 * @property Account $Account
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The Branch model refers to the physical location of the bank branch.
 */
class Branch extends AppModel {

    public $displayField = "name";

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => 'isUnique',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Branch name can not be empty or duplicated.'

            ),
        ),
        'phone' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Branch phone cannot be empty',
                'allowEmpty' => false,
            ),
        ),
    );

    //The Associations below have been created with all possible keys.

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Account' => array(
            'className' => 'Account',
            'foreignKey' => 'branch_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
