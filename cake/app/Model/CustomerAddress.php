<?php

App::uses('AppModel', 'Model');

/**
 * CustomerAddress Model
 *
 * @property Customer $Customer
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The Customer Address model refers to the physical or mailing
 * address of the Customer. A customer can have many addresses. Addresses
 * can only be added or removed via the Customer Profile. 
 * 
 */
class CustomerAddress extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'customer_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            ),
        ),
        'address_line_1' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            ),
        ),
        'suburb' => array(
            'suburb' => array(
                'rule' => array('notempty'),
            ),
        ),
        'post_code' => array(
            'notEmpty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter a valid post code'
            ),
            "validCode" => array(
                'rule' => '/^[0-9]{4}/',
                'message' => "Please enter a valid post code"
            ),
        ),
        'state' => array(
            'state' => array(
                'rule' => array('notempty'),
                'message' => 'Please select a valid state',
                'allowEmpty' => false
            ),
        ),
    );

    //The Associations below have been created with all possible keys.

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Customer' => array(
            'className' => 'Customer',
            'foreignKey' => 'customer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
