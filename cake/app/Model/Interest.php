<?php

App::uses('AppModel', 'Model');

/**
 * Interest Model
 *
 * @property AccountType $AccountType
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The Interest model refers to interest rates relating to the Account.
 * When an interest is created, the previous interest rate will be made inactive
 * by setting the end date to teh current date. When an interest is added, the 
 * open date is set to today and the end date is set to null.
 * An inactive interest is determined by the presence of the end date.
 * Interests are numeric. 
 */
class Interest extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'account_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            ),
        ),
    );

    //The Associations below have been created with all possible keys.

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'AccountType' => array(
            'className' => 'AccountType',
            'foreignKey' => 'account_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
