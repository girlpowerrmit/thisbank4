<?php

App::uses('AppModel', 'Model');

/**
 * Account Model
 * 
 * @property Branch $Branch
 * @property AccountType $AccountType
 * @property Customer $Customer
 * @property Transaction $Transaction
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The account model stores the customers bank account data
 */
class Account extends AppModel {

    public $displayField = "account_description";

    /**
     * Validation rules
     * Note: All fields for a new Account are mandatory.
     * @var array
     */
    public $validate = array(
        'branch_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Branch cannot be empty!',
            
            ),
        ),
        'active' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            
            ),
        ),
        'open_date' => array(
            'datetime' => array(
                'rule' => array('datetime'),
            
            ),
        ),
        'account_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Account type cannot be empty!',
 
            ),
        ),
        'account_description' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'required' => true,
                'message' => 'Account description cannot be empty!',

            ),
        ),
        'customer_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Customer cannot be empty!',
                
            ),
        ),
    );

    //The Associations below have been created with all possible keys.

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Branch' => array(
            'className' => 'Branch',
            'foreignKey' => 'branch_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AccountType' => array(
            'className' => 'AccountType',
            'foreignKey' => 'account_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Customer' => array(
            'className' => 'Customer',
            'foreignKey' => 'customer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'account_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
