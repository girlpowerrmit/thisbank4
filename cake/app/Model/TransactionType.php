<?php

App::uses('AppModel', 'Model');

/**
 * TransactionType Model
 *
 * @property Transaction $Transaction
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The transaction type refers to the type of transaction (debit & credit).
 */
class TransactionType extends AppModel {
    
    // The transaction type must not be empty and must be unique.
    
    public $validate = array(
        'description' => array(
            'rule' => 'isUnique',
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Transaction type description can not be empty and must be unique'
        )
    );
    
    //The Associations below have been created with all possible keys.

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'transaction_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
