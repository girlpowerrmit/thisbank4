<?php

App::uses('AppModel', 'Model');

/**
 * Transaction Model
 *
 * @property TransactionType $TransactionType
 * @property Account $Account
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: A transaction refers to the movement of funds to and from or between
 * accounts. Transactions can be credits or debits. The balance is the sum of the credits
 * minus the sum of the debits.
 */
class Transaction extends AppModel {
    //The Associations below have been created with all possible keys.

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'TransactionType' => array(
            'className' => 'TransactionType',
            'foreignKey' => 'transaction_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Account' => array(
            'className' => 'Account',
            'foreignKey' => 'account_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DestinationAccount' => array(
            'className' => 'Account',
            'foreignKey' => 'destination_account',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    
    // Calculates the balance of an account based on the credits minus the debits.
    public function Balance($accountId, $startDate = null) {

        if (empty($startDate)) {

            $startDate = date("Y-m-d", time() + 86400);
        }



        $transactions = $this->Find('all', array('conditions' => array('Account.id' => $accountId, 'Transaction.transaction_date <' => $startDate)));


        $balance = 0;
        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                if ($transaction['TransactionType']['id'] == 1) {
                    $balance+=$transaction['Transaction']['amount'];
                } else {
                    $balance-=$transaction['Transaction']['amount'];
                }
            }
        }



        return $balance;
    }

}
