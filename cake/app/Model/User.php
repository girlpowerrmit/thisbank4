<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 * @property Customer $Customer
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The user refers to the entity that accesses the system, 
 * with existing user roles being ADMIN and CUSTOMER. It is possible for 
 * future roles to be added. Roles will determine access to pages, and elements
 * in the interface. 
 */
class User extends AppModel {

    // Password hashing when a new user is created.
    
    public function beforeSave($options = array()) {

        if (isset($this->data[$this->alias]['password'])) {

            if (isset($this->data[$this->alias]['id'])) {
                $id = $this->data[$this->alias]['id'];
                $user = $this->findById($id);
            } else {
                $id = false;
            }

            if (!$id || $this->data[$this->alias]['password'] != $user['User']['password']) {
                $passwordHasher = new SimplePasswordHasher();
                $this->data[$this->alias]['password'] = $passwordHasher->hash(
                        $this->data[$this->alias]['password']
                );
            }
        }

        return true;
    }

    /* Validation rules for a user are as follows
        - a username must be unique
        - a user who is a customer must have an associated customer ID
        - only one user per customer
        - passwords must be alphanumeric
        - Usernames must be between 5 & 15 chars.
     
        
     */
    public $validate = array(
        'username' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required',
                'allowEmpty' => false
            ),
            'between' => array(
                'rule' => array('between', 5, 15),
                'required' => true,
                'message' => 'Usernames must be between 5 to 15 characters'
            ),
            'unique' => array(
                'rule' => array('isUniqueUsername'),
                'message' => 'This username is already in use'
            ),
            'alphaNumericDashUnderscore' => array(
                'rule' => array('alphaNumericDashUnderscore'),
                'message' => 'Username can only be letters, numbers and underscores'
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            ),
            'min_length' => array(
                'rule' => array('minLength', '6'),
                'message' => 'Password must have a mimimum of 6 characters'
            )
        ),
        'role' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'A role is required.',
            ),
        ),
        'customer_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'A customer is required',
                'allowEmpty' => false
            ),
            'unique' => array(
                'rule' => array('isUniqueCustomer'),
                'message' => 'This customer is already in use.'
            )),
    );
    // Validation rules for an Admin role

    public $validateForAdmin = array(
        // Customer validation doesn't fire for the Admin role
        'customer_id' => array(
            'empty' => array(
                'rule' => array('empty'),
                'allowEmpty' => true
            )
        ),
        // Apply unique username rule below
        'username' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required',
                'allowEmpty' => false
            ),
            'between' => array(
                'rule' => array('between', 5, 15),
                'required' => true,
                'message' => 'Usernames must be between 4 to 15 characters'
            ),
            'unique' => array(
                'rule' => array('isUniqueUsername'),
                'message' => 'This username is already in use'
            ),
        )
    );

    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
    function isUniqueUsername($check) {

        $username = $this->find(
                'first', array(
            'fields' => array(
                'User.id',
                'User.username'
            ),
            'conditions' => array(
                'User.username' => $check['username']
            )
                )
        );



        if (!empty($username)) {

            if ($this->data[$this->alias]['id'] == $username['User']['id']) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Before isUniqueCustomer
     * @param array $options
     * @return boolean
     */
    function isUniqueCustomer($check) {

        $customer = $this->find(
                'first', array(
            'fields' => array(
                'User.id'
            ),
            'conditions' => array(
                'User.customer_id' => $check['customer_id']
            )
                )
        );

        if (!empty($customer)) {
            if ($this->data[$this->alias]['id'] == $customer['User']['id']) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function alphaNumericDashUnderscore($check) {
        // $data array is passed using the form field name as the key
        // have to extract the value to make the function generic
        $value = array_values($check);
        $value = $value[0];

        return preg_match('/^[a-zA-Z0-9_ \-]*$/', $value);
    }

    //The Associations below have been created with all possible keys.

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Customer' => array(
            'className' => 'Customer',
            'foreignKey' => 'customer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
