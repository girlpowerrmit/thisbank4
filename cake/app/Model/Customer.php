<?php

App::uses('AppModel', 'Model');

/**
 * Customer Model
 *
 * @property Account $Account
 * @property CustomerAddress $CustomerAddress
 * @property User $User
 * 
 * Authors: Lara Wilson, Melissa Thompson, Christine Zhu
 * Purpose: The Customer refers to the person (individual) holding a bank account
 * with $this->$Bank.
 */
class Customer extends AppModel {

    public $virtualFields = array(
        'name' => 'CONCAT(Customer.first_name, " ", Customer.last_name)'
    );
    public $displayField = "name";

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'first_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            ),
        ),
        'last_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            ),
        ),
        'dob' => array(
            'date' => array(
                'rule' => array('date'),
            ),
        ),
        'phone' => array(
            'phone' => array(
                'rule' => '/^0[1-9]{1}[0-9]{8}/',
                'message' => 'Please enter a valid phone number',
                'allowEmpty' => true
            ),
        ),
        'mobile' => array(
            'mobile' => array(
                'rule' => '/^04[0-9]{8}/',
                'message' => 'Please enter a valid mobile number',
                'allowEmpty' => true
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => array('email', 'notempty'),
            ),
        ),
        'active' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            ),
        ),
    );

    //The Associations below have been created with all possible keys. 

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Account' => array(
            'className' => 'Account',
            'foreignKey' => 'customer_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CustomerAddress' => array(
            'className' => 'CustomerAddress',
            'foreignKey' => 'customer_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'customer_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
