<?php
/**
 * CustomerFixture
 *
 */
class CustomerFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'create_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'active' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 4),
		'person_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'CustPersonID_idx' => array('column' => 'person_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'create_date' => '2015-07-29 22:04:12',
			'active' => 1,
			'person_id' => 1
		),
	);

}
