<?php
/**
 * TransactionFixture
 *
 */
class TransactionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'transaction_type_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'account_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'transaction_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'amount' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '13,4'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'TransactionType_idx' => array('column' => 'transaction_type_id', 'unique' => 0),
			'TransactionAccount_idx' => array('column' => 'account_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'transaction_type_id' => 1,
			'account_id' => 1,
			'transaction_date' => '2015-07-29 22:04:46',
			'amount' => 1
		),
	);

}
