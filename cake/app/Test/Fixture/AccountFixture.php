<?php
/**
 * AccountFixture
 *
 */
class AccountFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'branch_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'active' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4),
		'open_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'account_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'customer_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'BranchAccount_idx' => array('column' => 'branch_id', 'unique' => 0),
			'CustomerAccount_idx' => array('column' => 'customer_id', 'unique' => 0),
			'AccountTypeAccount_idx' => array('column' => 'account_type_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'branch_id' => 1,
			'active' => 1,
			'open_date' => '2015-07-29 22:03:49',
			'account_type_id' => 1,
			'customer_id' => 1
		),
	);

}
