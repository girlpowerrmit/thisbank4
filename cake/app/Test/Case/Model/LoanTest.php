<?php
App::uses('Loan', 'Model');

/**
 * Loan Test Case
 *
 */
class LoanTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.loan',
		'app.loan_type',
		'app.account',
		'app.branch',
		'app.account_type',
		'app.customer',
		'app.person',
		'app.person_address',
		'app.user',
		'app.role',
		'app.transaction',
		'app.transaction_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Loan = ClassRegistry::init('Loan');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Loan);

		parent::tearDown();
	}

}
