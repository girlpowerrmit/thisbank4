<?php
App::uses('LoansController', 'Controller');

/**
 * LoansController Test Case
 *
 */
class LoansControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.loan',
		'app.loan_type',
		'app.account',
		'app.branch',
		'app.account_type',
		'app.customer',
		'app.person',
		'app.person_address',
		'app.user',
		'app.role',
		'app.transaction',
		'app.transaction_type'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
